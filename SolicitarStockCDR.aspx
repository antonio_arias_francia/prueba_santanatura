﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="SolicitarStockCDR.aspx.cs" Inherits="SantaNaturaNetworkV3.SolicitarStockCDR" ClientIDMode="Static" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <link href="assets/css/demo.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css">
    <link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />
    <link href="css/datatables/dataTables.bootstrap.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css" integrity="sha512-HK5fgLBL+xu6dm/Ii3z4xhlSUyZgTT9tuc/hSrtw6uzJOvgRr2a9jyxxT1ely+B+xFAmJKVSTbpM/CuL7qxO8w==" crossorigin="anonymous" />

    <style>
        .table > tbody > tr > td, .table > tbody > tr > th, .table > tfoot > tr > td,
        .table > tfoot > tr > th, .table > thead > tr > td, .table > thead > tr > th {
            vertical-align: middle;
        }

        .ui-front {
            z-index: 2000 !important;
        }

        .modal-body {
            overflow: hidden;
        }

            .modal-body:hover {
                overflow-y: auto;
            }

        /*------------------------------NEW MODAL REGISTRAR STOCK-----------------------------------*/
        .bloqueAgregarProd {
            display: grid;
            grid-template-columns: repeat(2, 1fr);
            grid-template-rows: repeat(2, 1fr);
            grid-gap: 10px;
        }

        .bloqueAgregarProd__dropdownProd {
            display: flex;
            grid-gap: 10px;
        }

        .bloqueAgregarProd__btnAgregar {
            text-align: center;
        }

        .flex {
            display: flex;
            flex-direction: column;
            align-items: center;
        }

        .registrarStock > .table-bordered > tbody > tr > td {
            border: none
        }

        .borderBottom {
            border-bottom: 1px solid #ddd !important;
        }



        @media (min-width: 541px) {
            #exampleModal2 .modal-dialog {
                max-height: calc(100vh - 60px) !important;
            }
        }

        @media (max-device-width: 540px) {
            #exampleModal2 .modal-dialog {
                margin: 0;
            }
        }

        @media (max-device-width: 500px) {
            #exampleModal2 .modal-dialog .modal-content {
                max-height: calc(100vh - 50px) !important;
            }
        }

        @media (max-device-width: 414px) {
            .centrado {
                display: block;
            }
        }

        @media (max-device-width: 375px) {
            .bloqueAgregarProd {
                display: grid;
                grid-template-columns: repeat(1, 1fr);
                grid-template-rows: repeat(4, 1fr);
            }

            .centrado {
                display: flex;
                justify-content: center;
                grid-gap: 10px;
            }

            .bloqueAgregarProd__dropdownProd {
                justify-content: center;
            }

            .saldoComi p, .saldoDispo p {
                font-size: 14px !important;
            }
        }

        @media (max-device-width: 280px) {
            .bloqueAgregarProd__dropdownProd {
                flex-direction: column;
                align-items: center;
            }

            .bloqueAgregarProd__btnAgregar {
                display: flex;
                align-items: center;
                justify-content: center
            }
            .centrado {
                align-items: center;
            }
        }

        /*Para quitar el efecto hover que viene por defecto en el boostrap a los tr*/
        .table-hover > tbody > tr:hover {
            background-color: white;
        }

        /*Para el input-number*/
        input[type=number]::-webkit-inner-spin-button,
        input[type=number]::-webkit-outer-spin-button {
            -webkit-appearance: none;
            margin: 0;
        }

        .custom-input-number {
            text-align: center;
            margin: 0 auto;
            float: left;
        }

        .cin-btn {
            background: transparent;
            border: 1px solid #b8b8b8;
            border-radius: 50%;
            background: transparent;
            display: block;
            margin: 0 auto;
            height: 3rem;
            width: 3rem;
            -webkit-transition: all 0.1s ease-in-out;
            -moz-transition: all 0.1s ease-in-out;
            transition: all 0.1s ease-in-out;
        }

        .cin-btn-1 {
            display: inline-block;
        }

        .cin-btn-small {
            height: 1.5rem;
            width: 1.5rem;
        }

        .cin-btn-md {
            height: 3rem;
            width: 3rem;
        }

        .cin-btn-lg {
            height: 7rem;
            width: 7rem;
        }

        .cin-btn:active {
            background: #00A65A;
            border: 1px solid #ffffff;
            color: #ffffff;
        }

        .cin-btn:focus {
            outline: none;
        }

        .cin-input {
            width: 30px;
            border: 0;
            box-shadow: inset 0 0 0 rgba(64, 150, 255, 0.1);
            -webkit-appearance: none;
            -moz-appearance: none;
            -ms-appearance: none;
            -o-appearance: none;
            appearance: none;
            background: transparent;
            margin: 1rem 0;
            pointer-events: none;
            text-align: center;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }
    </style>
    <section class="content-header">
        <h1 style="text-align: center">Generar Pedido</h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-success">
                    <div class="box box-header">
                        <h3 class="box-title">Lista de Pedidos</h3>
                    </div>
                    <div style="padding-left: 11px; margin-bottom: 40px; display: flex; justify-content: center" class="col-md-12">
                        <div class="col-md-4 saldoComi">
                            <p style="margin-top: auto; margin-bottom: auto; margin-right: 10px; margin-left: 10px; font-family: 'Dancing Script', cursive; font-size: 25px;">
                                Saldo Comisiones:
                        <asp:Label runat="server" ID="txtSaldoComision">0</asp:Label>
                            </p>
                        </div>
                        <div class="col-md-4 saldoDispo">
                            <p style="margin-top: auto; margin-bottom: auto; margin-right: 20px; font-family: 'Dancing Script', cursive; font-size: 25px;">
                                Saldo Disponible:
                        <asp:Label runat="server" ID="txtLineaCredito">0</asp:Label>
                            </p>
                        </div>
                    </div>
                    <div style="padding-left: 11px; margin-bottom: 30px" class="col-md-12">
                        <div class="col-md-2" style="margin-top: 15px; margin-bottom: 15px;">
                            <button runat="server" type="button" class="btn btn-success" style="font-weight: bold" id="btnModals">Crear Pedido</button>
                        </div>
                        <div class="col-md-2">
                            <label>Tipo Pago</label>
                            <asp:DropDownList runat="server" ID="cboTipoPago" CssClass="form-control" />
                        </div>
                    </div>
                    <!-- Modal -->
                    <div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="container modal-dialog modal-lg" id="modalTamano2" role="document" style="height: 100%; display: flex; align-items: center;">
                            <div class="modal-content" style="max-height: 700px; overflow-y: scroll; max-width: 1000px; width: 100%;">
                                <div class="modal-header">
                                    <h4 style="text-align: center; font-weight: bold" class="modal-title" id="exampleModalLabel2">Registrar Stock</h4>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <div class="modal-body1">
                                        <div class="bloqueAgregarProd">
                                            <%--                                            <div class="col-md-5">
                                                <label>Despacho</label>
                                                <asp:TextBox runat="server" ID="txtCDR" CssClass="form-control" BackColor="LightGreen" ReadOnly="true" />
                                            </div>--%>
                                            <div class="centrado">
                                                <label>Total Pagar:  </label>
                                                <asp:Label runat="server" ID="txtTotPagar" />
                                            </div>
                                            <div class="centrado">
                                                <label>Fecha: </label>
                                                <asp:Label runat="server" ID="txtFecha" />
                                            </div>
                                            <div id="dvSaldo" style="display:none;">
                                                <label>Saldo Disponible:  </label>
                                                <asp:Label runat="server" ID="SD" />
                                            </div>
                                            <div class="bloqueAgregarProd__dropdownProd">
                                                <label>Producto</label>
                                                <select style="box-shadow: 0 0 5px 5px aliceblue; width: 100%; background-color: white; color: #7d6754; font-family: Andalus" id="cboProductoCDR" class="ddl js-example-templating" runat="server"></select>
                                            </div>
                                            <div class="bloqueAgregarProd__btnAgregar">
                                                <button id="btnAgregar" type="button" class="btn btn-success">Agregar</button>
                                            </div>
                                        </div>
                                        <br />
                                        <div class="row">
                                            <div class="box-body table-responsive registrarStock">
                                                <table id="tbl_registro" class="table table-bordered table-hover text-center">
                                                    <tbody id="tbe2">
                                                        <%--<tr>
                                                            <td rowspan="2" class="borderBottom">
                                                                <img src="https://picsum.photos/52" alt="Alternate Text" />
                                                            </td>
                                                            <td>
                                                                <div class="flex">
                                                                    <label>Nombre</label>
                                                                    <asp:Label runat="server" ID="lblNombre" />asdasd
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div class="flex">
                                                                    <label>Cantidad</label>
                                                                    <div class="custom-input-number">
                                                                        <button type="button" class="cin-btn cin-btn-1 cin-btn-md cin-increment">
                                                                            +
                                                                        </button>
                                                                        <input type="number" class="cin-input basket-quantity" step="1" value="1" min="0" max="100">
                                                                        <button type="button" class="cin-btn cin-btn-1 cin-btn-md cin-decrement">
                                                                            -
                                                                        </button>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                            <td rowspan="2" class="borderBottom">
                                                                <button type="button" class="cin-btn cin-btn-1 cin-btn-md cin-increment">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="borderBottom">
                                                                <div class="flex">
                                                                    <label>Precio</label>
                                                                    <asp:Label runat="server" ID="lblPrecio" />58.52
                                                                </div>
                                                            </td>
                                                            <td class="borderBottom">
                                                                <div class="flex">
                                                                    <label>ID PS</label>
                                                                    <asp:Label runat="server" ID="lblIDPS" />285
                                                                </div>
                                                            </td>
                                                            
                                                        </tr>--%>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <table>
                                        </table>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button id="btnCancelar2" type="reset" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                                    <button id="btnRegistrar2" type="button" class="btn btn-success">Comprar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Modal -->
                    <!-- Modal -->
                    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="container modal-dialog" id="modalTamano" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 style="text-align: center; font-weight: bold" class="modal-title" id="exampleModalLabel">Detalle de Solicitud de Stock</h4>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <div class="modal-body1">
                                        <div class="row">
                                            <div class="form-group col-md-6">
                                                <div class="col-md-6">
                                                <label>YACHAY WASI: </label>
                                                </div>
                                                <div class="col-md-6">
                                                    <asp:Label runat="server" ID="lblNameCDR"/>
                                                </div>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <div class="col-md-6">
                                                <label>Monto Total:  </label>
                                                </div>
                                                <div class="col-md-6">
                                                <asp:Label runat="server" ID="lblMontoTo" />
                                                </div>
                                            </div>
                                        </div>
                                        <br />
                                        <div class="row">
                                            <div class="box-body table-responsive">
                                                <table id="tbl_detalle" class="table table-bordered table-hover text-center">
                                                    <thead>
                                                        <tr>
                                                            <th>Orden</th>
                                                            <th>Producto</th>
                                                            <th>Imagen</th>
                                                            <th>ID PS</th>
                                                            <th>Cantidad de Packs</th>
                                                            <th>Unidades</th>
                                                            <th>SubTotal</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody id="tbActualiza">
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button id="btnCancelar" type="reset" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Modal -->
                    <div class="box-body table-responsive">
                        <table id="tbl_cdr" class="table table-bordered table-hover text-center">
                            <thead>
                                <tr>
                                    <th>Fila</th>
                                    <th>Fecha Solicitud</th>
                                    <th>ID</th>
                                    <th>Tipo de Compra</th>
                                    <th>Monto</th>
                                    <th>Cantidad Total</th>
                                    <th>Ver Detalle</th>
                                </tr>
                            </thead>
                            <tbody id="tbl_body_table">
                                <!--Data por medio de Ajax-->
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <script src="../assets/js/jquery-ui.min.js" type="text/javascript"></script>
        <script src="js/sweetAlert.js" type="text/javascript"> </script>
        <script src="js/plugins/datatables/jquery.dataTables.js"></script>
        <script src="js/plugins/datatables/dataTables.bootstrap.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>
        <script src="js/SolicitarProductosCDRV2.js?v9"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                $(".js-example-templating").select2({
                    dropdownParent: $("#exampleModal2")
                });
            });
            

            /*Para el input number*/
            
            

        </script>

    </section>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footer" runat="server">
</asp:Content>
