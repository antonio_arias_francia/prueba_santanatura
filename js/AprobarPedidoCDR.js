﻿var tabla, data, tabla2, data2, data3;

tabla3 = $("#tbl_detalle").DataTable({
    "bPaginate": false,
    "bSort": false
});

tabla = $("#tbl_cdr").DataTable();

function sendDataAjax() {
    $.ajax({
        type: "POST",
        url: "AprobarPedidosCDR.aspx/ListarSolicitudesTotales",
        data: {},
        contentType: 'application/json; charset=utf-8',
        error: function (xhr, ajaxOptions, throwError) {
            console.log(xhr.status + " \n" + xhr.responseText, "\n" + thrownError);
        },
        success: function (data) {
            console.log(data.d);
            addRowDT(data.d);
        }
    });
}

function addRowDT(obj) {
    
    tabla.fnClearTable();
    for (var i = 0; i < obj.length; i++) {
        var btnAprob = "";
        var tipoCompra = "";
        if (obj[i].Estado == "Aprobado" || obj[i].TipoCompra != "3") { btnAprob = "style='display:none'"; }
        tipoCompra = (obj[i].TipoCompra === "1" ? "ADICIONAL" : obj[i].TipoCompra === "2" ? "EMERGENCIA" : "SALDO DISPONIBLE");
        tabla.fnAddData([
            obj[i].Fila,
            obj[i].FechaSolicitud,
            obj[i].CDRPS,
            obj[i].Estado,
            obj[i].MontoTotal,
            tipoCompra,
            '<button id="btnAbrirModal" value="Detalle" title="Detalle" type="button" class="btn btn-primary btn-deta" data-toggle="modal" data-target="#exampleModal"><i class="fas fa-search"></i></button>',
            '<button id="btnAprobar" value="Aprobar" title="Aprobar" type="button" class="btn btn-success btn-aprob"' + btnAprob + '><i class="fas fa-check"></i></button>',
            obj[i].IdSolicitud,
            obj[i].DNICDR
        ]);
    }
}

$(document).on('click', '.btn-aprob', function (e) {
    e.preventDefault();
    var row2 = $(this).parent().parent()[0];
    var datax = tabla.fnGetData(row2);
    aprobarSolicitud(datax[8], datax[9], datax[4], datax[1]);
});

function aprobarSolicitud(idSoli, dniCDR, monto, fecha) {

    var obj = JSON.stringify({ idSolicitud: idSoli, dni: dniCDR, fecha: fecha, monto: monto });
    $.ajax({
        type: "POST",
        url: "AprobarPedidosCDR.aspx/AprobarSolicitudCDR",
        data: obj,
        contentType: 'application/json; charset=utf-8',
        error: function (xhr, ajaxOptions, throwError) {
            console.log(xhr.status + " \n" + xhr.responseText, "\n" + thrownError);
        },
        success: function (data3) {
            console.log(data3.d);
            Swal.fire({
                title: 'Perfecto!',
                text: 'Solicitud Aprobada',
                type: "success"
            }).then(function () {
                window.location = "AprobarPedidosCDR.aspx";
            });
        }
    });
}

$(document).on('click', '.btn-deta', function (e) {
    e.preventDefault();
    var row2 = $(this).parent().parent()[0];
    var datax = tabla.fnGetData(row2);
    $("#txtCDRDetalle").val(datax[2]);
    sendDataAjaxDetalle(datax[8]);
});

function sendDataAjaxDetalle(idSoli) {

    var obj = JSON.stringify({ idSolicitud: idSoli });
    $.ajax({
        type: "POST",
        url: "AprobarPedidosCDR.aspx/ListarProductosxSolicitud",
        data: obj,
        contentType: 'application/json; charset=utf-8',
        error: function (xhr, ajaxOptions, throwError) {
            console.log(xhr.status + " \n" + xhr.responseText, "\n" + thrownError);
        },
        success: function (data2) {
            console.log(data2.d);
            addRowDTDetalle(data2.d);
        }
    });
}

function addRowDTDetalle(obj) {
    tabla3.fnClearTable();
    for (var i = 0; i < obj.length; i++) {
        var imagenProducto = obj[i].Imagen;
        var cant = obj[i].Cantidad;
        tabla3.fnAddData([
            obj[i].Fila,
            obj[i].NombreProducto,
            '<img src="products/' + imagenProducto + '" style="height: 80px">',
            obj[i].IDPS,
            '<input type="text" id="cantA' + i + '" name="" class="form-control daterange" style="background-color: lightgreen; width:90px" readonly value="' + cant + '">'
        ]);
    }
}

$("#btnFiltrar").click(function (e) {
    e.preventDefault();
    var fecha1 = $("#fechaInicio").val();
    var fecha2 = $("#fechaFin").val();
    sendDataAjaxFiltro(fecha1, fecha2);
});

function sendDataAjaxFiltro(fecha1, fecha2) {
    var obj = JSON.stringify({ fecha1: fecha1, fecha2: fecha2 });
    $.ajax({
        type: "POST",
        url: "AprobarPedidosCDR.aspx/ListarSolicitudesTotalesFiltro",
        data: obj,
        contentType: 'application/json; charset=utf-8',
        error: function (xhr, ajaxOptions, throwError) {
            console.log(xhr.status + " \n" + xhr.responseText, "\n" + thrownError);
        },
        success: function (data) {
            addRowDT(data.d);
        }
    });
}

sendDataAjax();