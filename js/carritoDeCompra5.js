﻿function fijarCampoRequeridoRegistrarCliete() {
    $("[id$=txtUl]").prop("required", true);
    $("[id$=TxtCl]").prop("required", true);
    $("[id$=txtNombre]").prop("required", true);
    $("[id$=txtApPaterno]").prop("required", true);
    $("[id$=txtApMaterno]").prop("required", true);
    $("[id$=txtNumDocumento]").prop("required", true);
    $("[id$=TxtUsuario]").prop("required", true);
    $("[id$=cboTipoCliente]").prop("required", true);
    $("[id$=datepicker]").prop("required", true);
    $("[id$=ComboTipoDocumento]").prop("required", true);
    $("[id$=ComboSexo]").prop("required", true);
    $("[id$=TxtClave]").prop("required", true);
    $("[id$=TxtCorreo]").prop("required", true);
    $("[id$=cboPremio]").prop("required", true);
}

function quitarCampoRequeridoRegistrarCliete() {
    $("[id$=txtUl]").prop("required", true);
    $("[id$=TxtCl]").prop("required", true);
    $("[id$=txtNombre]").prop("required", false);
    $("[id$=txtApPaterno]").prop("required", false);
    $("[id$=txtApMaterno]").prop("required", false);
    $("[id$=txtNumDocumento]").prop("required", false);
    $("[id$=TxtUsuario]").prop("required", false);
    $("[id$=TxtClave]").prop("required", false);
    $("[id$=cboTipoCliente]").prop("required", false);
    $("[id$=datepicker]").prop("required", false);
    $("[id$=ComboTipoDocumento]").prop("required", false);
    $("[id$=ComboSexo]").prop("required", false);
    $("[id$=TxtCorreo]").prop("required", false);
    $("[id$=TxtCelular]").prop("required", false);
    $("[id$=cboPremio]").prop("required", false);
}

function MostrarRegistro() {
    var divRegistroCliente = document.getElementById('MostrarRegistroCliente');

    divRegistroCliente.style.display = "block";
    fijarCampoRequeridoRegistrarCliete();
}

$("#ButtonValida").click(function (e) {
    e.preventDefault();
    var ruc = $("#TextBoxRUC").val();
    validarRUC(ruc.trim());
}); 

function validarRUC(numruc) {
    var obj = JSON.stringify({
        ruc: numruc
    });
    $.ajax({
        type: "POST",
        url: "https://servicio.apirest.pe/api/getRuc",
        beforeSend: function (xhr) {
            xhr.setRequestHeader('Authorization', 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjgzMmUzYTI3ZDA0YzI2ZjFmNTIwNTJhMjE5ODRmOTI1NDY2ZmM1YjA2MzcyOWNiNDc4NjExM2VlYzRjZDA5NjY2OTRjNzEzY2U5YTNjNmMyIn0.eyJhdWQiOiIxIiwianRpIjoiODMyZTNhMjdkMDRjMjZmMWY1MjA1MmEyMTk4NGY5MjU0NjZmYzViMDYzNzI5Y2I0Nzg2MTEzZWVjNGNkMDk2NjY5NGM3MTNjZTlhM2M2YzIiLCJpYXQiOjE1ODk5Mjc5MTAsIm5iZiI6MTU4OTkyNzkxMCwiZXhwIjoxOTA1NDYwNzEwLCJzdWIiOiIxMDcyIiwic2NvcGVzIjpbIioiXX0.b_D5SKP8vifhZ38XY88NLKoO_0djb42S8KRqG76ZRLoSYI8f_Y4hEbGiyA0lmEk1miTd5I7_qbE7VPYIYpwy4CxaEN2lVJaqyTTnjupALsDiV0i2a5JMZ5kklXs-HdE6m7Ikovax-ZREcRMTKHxcpAIznYvcL-j3lutmlXRLoUVpRdyWFpVLGpWLagxhu7wHIrURW3ssGZPZCcv6hci3Xs9y8iJUKCt1AU1oNRBiwoL25O-4740jrQRzLLX2PP5KnHsFVNxy79y4LeC4zlhpPmpAuiJKi4KnHj4m3A0oBy9mql8rlCXzGGK9pyd8XjiO4gpiqa82xaYSawk6F6M6h8RkVAaEXZKeFuofDIQjKsp0QClw6egRA24TA-j_ZyvzDX5HzTG6y_1-H1zjPhSmVPH-3fsU8YZ52EFQcHJ28m3AaEDR9NOmaOgy-nLEIvAjunFMc67tO8A6f4I6QInnpcexW4S6FHcDT3Dws3v38BBCac31Pkmn4OKNl4ymIX37h-cztmBKC-tUDFI5Xssy26GxRPt-7NRlO-3BSU9HPEGke855TaigjnObSf6opHUxR9NjDzqVecsEAtNsz9cgozX_EqyGhRiSH2YwnYFOp-IU22v3SI1VNu0ijd263pUhVFfH1JM1OtqWVU8-vOLSC68tvFsF0ylS2HT3LO96U2Q');
        },
        data: obj,
        contentType: 'application/json; charset=utf-8',
        error: function (xhr, ajaxOptions, throwError) {
            console.log(xhr.status + " \n" + xhr.responseText, "\n" + thrownError);
        },
        success: function (data) {
            var success = data.success;
            var estado = data.result.Estado;
            if (success == true /*&& estado == "ACTIVO"*/) {
                var RS = data.result.RazonSocial;
                ValidacionOK(RS, numruc);
            } else {
                ErrorRUC();
            }
            
            
        }
    });
}

function ErrorRUC() {
    Swal.fire({
        type: 'error',
        title: 'Oops...',
        text: 'El RUC ingresado es inválido o está Inactivo',
    })
}

function ValidacionOK(RS, numruc) {
    var obj = JSON.stringify({
        ruc: numruc
    });
    $.ajax({
        type: "POST",
        url: "DetalleDeCompra.aspx/ValidarRUC",
        data: obj,
        contentType: 'application/json; charset=utf-8',
        error: function (xhr, ajaxOptions, throwError) {
            console.log(xhr.status + " \n" + xhr.responseText, "\n" + thrownError);
        },
        success: function (data2) {
            console.log(data2.d);
            Swal.fire({
                title: 'RUC Válido!',
                text: 'Al continuar con la compra está aceptando que los datos de facturación estarán a nombre de:' + RS,
                type: "success"
            }).then(function () {
            });
        }
    });
}

function MostrarRegistroCliente(value) {
    GuardarDatosDelivery2();
    var dropdown = value/*value.options[value.selectedIndex].value*/;
    if (dropdown == "07" | dropdown == "08" | dropdown == "09" | dropdown == "10" |
        dropdown == "11" | dropdown == "12" | dropdown == "13") {
        //document.getElementById("circle02").hide;
        $("#circle02").hide();
        $("#circulito").text("02");
    }
    else {
        $("#circle02").show();
        $("#circulito").text("03");
    }

    var divRegistroCliente = document.getElementById('MostrarRegistroCliente');

    if (value == 1 || value == 2 || value == 3 || value == 4 || value == 5 || value == 6) {
        divRegistroCliente.style.display = "block";
        fijarCampoRequeridoRegistrarCliete();
    }
    else {
        divRegistroCliente.style.display = "none";
        quitarCampoRequeridoRegistrarCliete();
    }

    location.href = ("DetalleDeCompra.aspx?sTipoCom=" + value);
}

function GuardarDatosDelivery2() {

    var nombreD = $("#txtNombreDatosCompra").val();
    var DNID = $("#txtDNIDatosCompra").val();
    var CelularD = $("#txtCelularDatosCompra").val();
    var DireccionD = $("#txtDireccionDatosCompra").val();
    var NomTransporteD = $("#txtTransporteDatosCompra").val();
    var DirecTransporteD = $("#txtDirecTransporteDatosCompra").val();
    var ProvinciaD = $("#txtProvinciaDatosCompra").val();
    var DirecProvinciaD = $("#txtDirecProvinciaDatosCompra").val();
    var obj2 = JSON.stringify({
        Nombre: nombreD, DNI: DNID, Celular: CelularD, Direccion: DireccionD,
        Transporte: NomTransporteD, DirTransporte: DirecTransporteD, Provincia: ProvinciaD, DirProvincia: DirecProvinciaD
    });

    $.ajax({
        type: "POST",
        url: "DetalleDeCompra.aspx/GuardarDatosDelivery",
        data: obj2,
        contentType: 'application/json; charset=utf-8',
        error: function (xhr, ajaxOptions, throwError) {
            console.log(xhr.status + " \n" + xhr.responseText, "\n" + thrownError);
        },
        success: function (data2) {
            console.log(data2.d);
        }
    });
}

$(".tablaSiguiente").click(function (e) {
    GuardarDatosDelivery2();
    var StipoC = $("#STipoCompra").val();
    if (StipoC == "07" | StipoC == "08" | StipoC == "09" | StipoC == "10" |
        StipoC == "11" | StipoC == "12" | StipoC == "13") {
        var ordenCompletada = document.getElementById("ordenCompletada");
        var elemento = document.getElementById("circle03");
        $("#03ordenCompletada").fadeIn(300);
        $("#02afiliacion").slideUp(0);
        $("#01carritoDeCompras").slideUp(0);
        $("#resumenDeLaCompra").slideUp(0);
        $("#tablaCompra").slideUp(0);
        elemento.className += " active"
        ordenCompletada.className += " active"
    } else {
        var afiliacion = document.getElementById("afiliacion");
        var elemento = document.getElementById("circle02");
        $("#tablaCompra").slideUp(0);
        $("#resumenDeLaCompra").slideUp(0);
        $("#02afiliacion").fadeIn(300);
        elemento.className += " active";
        afiliacion.className += " active";
    }


});


function MostrarComboTiendaAndDatosDelivery(value) {
    var divComboTienda = document.getElementById('MostrarComboTiena');
    var divDatosDelivery = document.getElementById('MostrarDatosParaDelivery');

    if (value == "1") {
        divComboTienda.style.display = "block";
        divDatosDelivery.style.display = "none"
    }
    else {
        divComboTienda.style.display = "none";
        divDatosDelivery.style.display = "block"
    }
}

function validarLetras(e) {
    var keyCode = (e.keyCode ? e.keyCode : e.which);
    if (keyCode > 47 && keyCode < 58 || keyCode > 95 && keyCode < 107) {
        e.preventDefault();
    }
}

function validarNumeros(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
}





