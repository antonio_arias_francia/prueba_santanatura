﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="DatosPersonalesCDR.aspx.cs" Inherits="SantaNaturaNetworkV3.DatosPersonalesCDR" ClientIDMode="Static" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css">
    <link href="css/datatables/dataTables.bootstrap.css" rel="stylesheet" />
    <section class="content-header">
        <h1 style="text-align: center">DATOS PERSONALES</h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-6">
                <div class="box box-primary">
                    <div class="box-body">
                        <div class="form-group">
                            <label>DOCUMENTO DE IDENTIDAD</label>
                        </div>
                        <div class="form-group">
                            <asp:TextBox ID="txtDocumento" runat="server" CssClass="form-control solo-numero" MaxLength="8" ReadOnly="true"></asp:TextBox>
                        </div>
                        <div class="form-group">
                            <label>NOMBRES</label>
                        </div>
                        <div class="form-group">
                            <asp:TextBox ID="txtNombres" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                        </div>
                        <div class="form-group">
                            <label>APELLIDO PATERNO</label>
                        </div>
                        <div class="form-group">
                            <asp:TextBox ID="txtApPaterno" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="box box-primary">
                    <div class="box-body">
                       <div class="form-group">
                            <label>APELLIDO MATERNO</label>
                        </div>
                        <div class="form-group">
                            <asp:TextBox ID="txtApMaterno" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                        </div>
                        <div class="form-group">
                            <label>SEUDONIMO</label>
                        </div>
                        <div class="form-group">
                            <asp:TextBox ID="txtApodo" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="box box-primary">
                    <div class="box box-header">
                        <h3 class="box-title">DATOS PARA COMPRA EXTORNO</h3>
                    </div>
                    <div class="box-body">
                        <div class="form-group">
                            <label>DOCUMENTO DE IDENTIDAD / RUC</label>
                        </div>
                        <div class="form-group">
                            <asp:TextBox ID="txtDocExtorno" runat="server" CssClass="form-control solo-numero" ReadOnly="true"></asp:TextBox>
                        </div>
                        <div class="form-group">
                            <label>NOMBRES Y APELLIDOS / RAZON SOCIAL</label>
                        </div>
                        <div class="form-group">
                            <asp:TextBox ID="txtRazonExtorno" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                        </div>
                        <div class="form-group">
                            <label>DIRECCION / DIRECCION FISCAL</label>
                        </div>
                        <div class="form-group">
                            <asp:TextBox ID="txtDireccionExtorno" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div align="center">
            <table>
                <tr>
                    <td>
                        <asp:Button ID="btnActualizar" runat="server" CssClass="btn btn-primary" Width="200px" Text="Actualizar" />
                    </td>
                </tr>
            </table>
        </div>
        <script src="js/sweetAlert.js" type="text/javascript"> </script>
        <script src="js/DatosPersonalesCDR.js?v3"></script>
        <script type="text/javascript">
            window.onload = function () {
                $('.solo-numero').numeric();
                document.getElementById("GestionarCDR").classList.add("active");
            }
        </script>

    </section>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footer" runat="server">
</asp:Content>
