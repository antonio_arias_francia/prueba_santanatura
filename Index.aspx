﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Home.Master" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="SantaNaturaNetwork.Index" ClientIDMode="Static" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <!-- Icon css link -->
    <link href="css/proyecto2/font-awesome.min.css" rel="stylesheet" />
    <link href="css/proyecto2/vendors/line-icon/css/simple-line-icons.css" rel="stylesheet" />
    <%--<link href="css/proyecto2/vendors/elegant-icon/style.css" rel="stylesheet" />--%>

    <!-- Extra plugin css -->
    <link href="css/proyecto2/vendors/owl-carousel/owl.carousel.min.css" rel="stylesheet" />
    <link href="css/proyecto2/vendors/bootstrap-selector/css/bootstrap-select.min.css" rel="stylesheet" />

    <link href="css/proyecto2/responsive-v3.css" rel="stylesheet" />

    <!--******************************BEGIN CIRCLE SLIDER *******************************-->
    <%--<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" />--%>
    <!--******************************END CIRCLE SLIDER *******************************-->

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />

    <!--BEGIN:Para los videos de los socios-->
    <link href="css/proyecto2/estilos-videos4.css?v9" rel="stylesheet" />
    <%--<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">--%><!--11/11/19-->
    <!--END:Para los videos de los socios-->

    <!--Socios Destacados-->
    <link href="css/proyecto2/estilos-sociosDestacados.css" rel="stylesheet" />


    <link href="css/estilosVideosSocios.css" rel="stylesheet" />

    <link href="css/proyecto2/fondoCuerpoLogin.css" rel="stylesheet" />

    <style>
        .parallax1 {
            /* The image used */
            background: url("https://tienda.mundosantanatura.com/img/IMAGEN-DE-BIENVENIDA-1920x1080.jpg");
            /* Set a specific height */
            /* Create the parallax scrolling effect */
            background-attachment: fixed;
            background-position: top;
            background-repeat: no-repeat;
            position: relative;
            z-index: 2;
            background-size: 100vw 100vh;
            /*margin-top:-20px*/
        }

        .parallax2 {
            /* The image used */
            background: url("https://tienda.mundosantanatura.com/img/768 x 1024.png");
            /* Set a specific height */
            /* Create the parallax scrolling effect */
            background-attachment: fixed;
            background-position: top;
            background-repeat: no-repeat;
            position: relative;
            z-index: 2;
            background-size: 100vw 100vh;
            /*margin-top:-20px*/
        }

        .parallax3 {
            /* The image used */
            background: url("https://tienda.mundosantanatura.com/img/imagen10.jpg");
            /* Set a specific height */
            /* Create the parallax scrolling effect */
            background-attachment: fixed;
            background-position: bottom;
            background-repeat: no-repeat;
            background-size: cover;
        }

        .round {
            width: 200px
        }








        .carousel-control {
            padding-top: 7%;
            /*width: 5%;*/
        }

        .cuerpo {
            height: 185px;
        }



        .l_product_slider .l_product_slider .owl-prev, .l_product_slider .l_product_slider .owl-next, .l_product_slider .home_l_product_slider .owl-prev, .l_product_slider .home_l_product_slider .owl-next, .home_l_product_slider .l_product_slider .owl-prev, .home_l_product_slider .l_product_slider .owl-next, .home_l_product_slider .home_l_product_slider .owl-prev, .home_l_product_slider .home_l_product_slider .owl-next {
            height: 35px;
            width: 35px;
            margin-bottom: -100px;
            text-align: center;
            font-size: 23px;
            color: #000;
            border: 1px solid #cccccc;
            line-height: 36px;
            display: inline-block;
            -webkit-transition: all 400ms linear 0s;
            -o-transition: all 400ms linear 0s;
            transition: all 400ms linear 0s;
            cursor: pointer;
        }

            .l_product_slider .l_product_slider .owl-prev:hover, .l_product_slider .l_product_slider .owl-next:hover, .l_product_slider .home_l_product_slider .owl-prev:hover, .l_product_slider .home_l_product_slider .owl-next:hover, .home_l_product_slider .l_product_slider .owl-prev:hover, .home_l_product_slider .l_product_slider .owl-next:hover, .home_l_product_slider .home_l_product_slider .owl-prev:hover, .home_l_product_slider .home_l_product_slider .owl-next:hover {
                background: #000;
                color: #fff;
                border-color: #000;
            }


        .displayBlock {
            display: block
        }

        .displayTable {
            display: table
        }



        .claseFuente1 {
            font-family: 'Conv_angelina',Sans-Serif;
        }

        .claseFuente2 {
            font-family: 'HurmeGeometricSans1 LightOblique' !important;
            letter-spacing: 0.25em;
            color: #141438 !important;
        }

        .claseFuente3 {
            font-family: 'HurmeGeometricSans1' !important;
            color: #141438;
            font-size: 20px;
            text-align: initial;
        }

        .claseFuente4 {
            font-family: 'HurmeGeometricSans1 BlackOblique';
            color: #141438;
            font-size: 20px;
            text-align: initial;
        }

        .claseFuente5 {
            font-family: 'MASQUE__';
            color: #262660;
            font-size: 20px;
            text-align: initial;
        }

        .claseFuente6 {
            font-family: 'HurmeGeometricSans1 Oblique';
            color: #262660;
            font-size: 20px;
            text-align: initial;
        }

        .bloqueDatos {
            display: flex;
            justify-content: space-around;
            height: 85px;
            align-items: center;
        }

        .textPeriodo {
            line-height: 2;
        }

        .bloqueDatos {
            background: #262660;
            position: relative;
            box-shadow: 5px 5px 10px 0px #224058;
        }

            .bloqueDatos::before {
                height: 3px;
                width: 100%;
                bottom: 0;
                background: linear-gradient(90deg,transparent 20%, white 50%, transparent 80%);
                display: block;
                position: absolute;
                left: 0;
                content: "";
            }

            .bloqueDatos::after {
                height: 100%;
                width: 6px;
                top: 0;
                background: radial-gradient(white, transparent 50%);
                display: block;
                position: absolute;
                right: 0;
                content: "";
            }

        .bloqueIzquierda {
            font-size: calc(1rem + 1vw);
            text-align: center;
            color: white
        }

        .bloqueDerecha {
            font-size: 1.3vw;
            text-align: center;
            color: white
        }

    </style>


    <%--<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css2">--%><!--11/11/19-->
    <%--<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>--%>
    <%--<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>--%>


    <!-- Efectos de opacity por bloque-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="js/proyecto2/eskju.jquery.scrollflow.min.js"></script>
    <!--Para los progress bars-->
    <script src="js/proyecto2/opacidadYEfectoProgressBar.js"></script>

    <!-- Start WOWSlider.com HEAD section -->
    <link rel="stylesheet" type="text/css" href="engine1/style.css" />
    <script type="text/javascript" src="engine1/jqueryv2.js"></script>
    <!-- End WOWSlider.com HEAD section -->



    <link rel="stylesheet" type="text/css" href="css/proyecto2/plugin.css">


    <link href="css/proyecto2/circleProgressBar.css" rel="stylesheet" />

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


<%--<div runat="server" class="popup" id="modal" style="display: none">
        <div style="display: flex; justify-content: center; align-items: center; height: 100vh">
            <div class="modal-dialog modal-xl" style="width: 100%">
                <div class="modal-content">
                    <a style="position: absolute; top: 100px" href="javascript:;" onclick="toggleVideo('hide');">
                        <input type="checkbox" id="cerrar" />
                        <label for="cerrar" id="btn-cerrar">X</label>
                    </a>
                    <div id="popupVid" class="contenido embed-responsive embed-responsive-4by3">
                        <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/dLdpUTS2scw?enablejsapi=1" id="iframeVideo" frameborder="0" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>--%>

    <div runat="server" id="modal" style="display: none">
        <input type="checkbox" id="cerrar" />
        <label for="cerrar" id="btn-cerrar">X</label>
        <div class="popup">

            <div class="contenido">
                <img src="https://tienda.mundosantanatura.com/img/PopUp-18-02-2021.jpg?v3" alt="" id="imgPopup" />
            </div>
        </div>
    </div>




    <div id="bloqueNoLogueado" class="main_slider_area parallax1" style="height: 100vh; display: block" runat="server">
        <%--<div id="main_slider">
            <ul>
                <li style="border: 3px solid blue; width: 100%; height: 100vh">
                    <!-- MAIN IMAGE -->
                    <img src="img/imagen6.jpg" style="width:100vw;height:100vh" />

                    <!-- LAYER NR. 1 -->
                    
                </li>
            </ul>
        </div>--%>
    </div>


    <!--================Our Latest Product Area =================-->
    <div id="fondoIndexBody" style="background: url('https://tienda.mundosantanatura.com/img/FONDO-1920x1378.jpg'); background-size: 100vw 100%; background-repeat: no-repeat; z-index: 1;">

        <div id="bloqueLogueado" style="display: none" runat="server">
            <div id="wowslider-container1" style="margin-top: 70px">
                <div class="ws_images">
                    <ul>
                        <% foreach (var banners in ListaBanners)
                                        {%>
                        <li>
                            <img src="https://tienda.mundosantanatura.com/banners/<%=banners.Archivo%>" alt="<%=banners.Nombre%>" title="" id="<%=banners.ID_DATOS%>" /></li>
                        <% } %>
                    </ul>
                </div>
                <%--<div class="ws_bullets">
                    <div>
                        <a href="#" title="colageno"><span>
                            <img src="imagenes/tooltips/1920x400premiomarzo2.png" alt="colageno" />1</span></a>
                        <a href="#" title="colageno"><span>
                            <img src="imagenes/tooltips/1920x400premiomarzo2.png" alt="colageno" />2</span></a>
                    </div>
                </div>--%>

            </div>

            <section class="feature_product_area" style="margin-top: 60px; padding-bottom: 0px !important;">
                <%--<div class="container" id="bloquePeriodo" style="background: radial-gradient(circle, white 40%, green, transparent); display: flex">--%>
                <div class="container" id="bloquePeriodo" style="display: flex; max-width: 80%;">
                    <div id="bloqueIzquierda" class="col-xs-3" style="background-color: white">

                        <div class="bloqueDatos columnaIzquierda" style="margin-top: 10px">
                            <h4 id="txtPP" class="textPeriodo claseFuente4 bloqueIzquierda">PP</h4>

                            <h4 id="txtValorPP" class="textPeriodo claseFuente4 resultadoPeriodo bloqueIzquierda"><%=PP %></h4>
                        </div>
                        <div class="saltoLinea"></div>
                        <div class="bloqueDatos columnaIzquierda">
                            <h4 id="txtVP" class="textPeriodo claseFuente4 bloqueIzquierda">VP</h4>

                            <h4 id="txtValorVP" class="textPeriodo claseFuente4 resultadoPeriodo bloqueIzquierda"><%=VP %></h4>
                        </div>
                        <div class="saltoLinea"></div>
                        <div class="bloqueDatos columnaIzquierda">
                            <h4 id="txtVG" class="textPeriodo claseFuente4 bloqueIzquierda">VG</h4>

                            <h4 id="txtValorVG" class="textPeriodo claseFuente4 resultadoPeriodo bloqueIzquierda"><%=VG %></h4>
                        </div>
                        <div class="saltoLinea"></div>
                        <div class="bloqueDatos columnaIzquierda">
                            <h4 id="txtVQ" class="textPeriodo claseFuente4 bloqueIzquierda">VQ</h4>

                            <h4 id="txtValorVQ" class="textPeriodo claseFuente4 resultadoPeriodo bloqueIzquierda"><%=VQ %></h4>
                        </div>

                    </div>

                    <div class="col-xs-6" id="bloqueDelMedio" style="height: auto; display: flex; align-items: center;">
                        <div id="bloqueImagenVamosPorMas" class="col-xs-12" style="height: auto">
                            <%--<img src="img/frase.png" style="margin-left: auto; margin-right: auto; display: block; width: inherit; margin-top: 10px;" />--%>
                            <h1 class="claseFuente5" style="text-align: center; margin-left: auto; margin-right: auto; font-weight: bold; font-size: calc(1rem + 2.2vw);">¡Vamos por tu<br />
                                siguiente<br />
                                conquista!
                            </h1>
                        </div>
                    </div>
                    <div id="bloqueDerecha" class="col-xs-3 " style="background-color: white">
                        <!-- box-shadow: goldenrod 0px 0px 20px; -->
                        <div class="bloqueDatos columnaDerecha" style="margin-top: 10px">
                            <h4 id="txtRANGO" class="textPeriodo claseFuente4 bloqueDerecha">RANGO</h4>

                            <h4 id="txtValorRANGO" class="textPeriodo claseFuente4 resultadoPeriodo bloqueDerecha"><%=RANGO %></h4>
                        </div>
                        <div class="saltoLinea"></div>
                        <div class="bloqueDatos columnaDerecha">
                            <h4 id="txtCOMISION" class="textPeriodo claseFuente4 bloqueDerecha">COMISION</h4>

                            <h4 id="txtValorCOMISION" class="textPeriodo claseFuente4 resultadoPeriodo bloqueDerecha"><%=COMISION %></h4>
                        </div>
                        <div class="saltoLinea"></div>
                        <div class="bloqueDatos columnaDerecha">
                            <h4 id="txtVIP" class="textPeriodo claseFuente4 bloqueDerecha">VIP</h4>

                            <h4 id="txtValorVIP" class="textPeriodo claseFuente4 resultadoPeriodo bloqueDerecha"><%=VIP %></h4>
                        </div>
                        <div class="saltoLinea"></div>
                        <div class="bloqueDatos columnaDerecha">
                            <h4 id="txtNUEVOSDIRECTOS" class="textPeriodo claseFuente4 nuevosDirectos" style="text-align: center; color: white; font-size: 1.3vw; width: 110px; line-height: 1; margin-top: auto; margin-bottom: auto;">NUEVOS DIRECTOS</h4>

                            <h4 id="txtValorNUEVOSDIRECTOS" class="textPeriodo claseFuente4 resultadoPeriodo nuevosDirectos" style="text-align: center; color: white; font-size: 1.6vw; margin-top: auto; margin-bottom: auto;"><%=DIRECTOS %></h4>
                        </div>

                    </div>
                </div>
            </section>

            <section id="sectionSiguienteRango" style="margin-top: 50px">
                <div class="container">

                    <div class="container">
                        <div class="row" style="display: flex; justify-content: center">
                                <%--<div class="my-progress-bar"></div>--%>
                                <svg id="valuePercentage" class="radial-progress" viewBox="0 0 80 80">
                                    <circle class="incomplete" cx="40" cy="40" r="35"></circle>
                                    <circle class="complete" cx="40" cy="40" r="35" style="stroke-dashoffset: 50;"></circle>
                                    <text class="percentage" x="50%" y="50%" transform="matrix(0, 1, -1, 0, 80, 0)" style="font-size: 20px;"><%=PORCENTAJE %>%</text>
                                    <text class="percentage" x="50%" y="65%" transform="matrix(0, 1, -1, 0, 80, 0)" style="font-size: 5px;"><%=RANGOPROXIMO %></text>
                                </svg>
                        </div>
                    </div>




                    <div id="bloqueSiguienteRango" class="col-xs-12" style="padding: 0 0 50px 0; display: none">
                        <%--<div id="siguienteRango" style="text-align: center; font-size: 30px; color: darkgoldenrod; font-family: 'Jacques Francois', serif; width: inherit; background: radial-gradient(circle,white 40%, transparent); padding-top: 10px; padding-bottom: 15px;">--%>
                        <div style="padding: 10px 0 15px; display: flex; justify-content: center">
                            <p class="claseFuente4 siguienteRango" style="color: #262660; font-size: 40px; line-height: 1; text-align: center;">
                                <%=RANGOPROXIMO %>
                            </p>
                        </div>
                        <div class="progress" style="box-shadow: 5px 5px 10px rgba(0,0,0,.8); background-color: white">
                            <div id="progressbar1" class="progress-bar progress-bar-striped progress-bar-info active  scrollflow -slide-right " role="progressbar" style="background-color: #262660">
                                <span class="current-value">0%</span>
                            </div>
                        </div>
                    </div>

                    <div id="bloqueNuevasAfiliacionesRed">
                        <div class="claseFuente4 nuevasAfiliacionesRed" style="text-align: center; color: #262660">
                            NUEVAS AFILIACIONES RED = <%=AFILIACIONES_RED %>
                        </div>
                        <div class="progress" style="box-shadow: 5px 5px 10px rgba(0,0,0,.8); background-color: white; display: none;">
                            <div id="progressbar3" class="progress-bar progress-bar-striped progress-bar-info active scrollflow -slide-right " role="progressbar" style="background-color: #262660">
                                <span class="current-value">0%</span>
                            </div>
                        </div>
                    </div>


                    <div id="bloqueTotalInscritos">
                        <%--<div class="claseFuente4" style="text-align: center; color: #262660">
                            TOTAL INSCRITOS = 120
                        </div>--%>
                        <div class="claseFuente4" style="text-align: center; color: #262660">
                            ACTIVOS RED <%=ACTIVOS_SOCIOS %>/<%=TOTALSOCIOS %>
                        </div>
                        <div class="progress" style="box-shadow: 5px 5px 10px rgba(0,0,0,.8); background-color: white">
                            <div id="progressbar2" class="progress-bar progress-bar-striped progress-bar-info active  scrollflow -slide-right " role="progressbar" style="background-color: #262660">
                                <span class="current-value">0%</span>
                            </div>
                        </div>
                    </div>

                    <div style="display: none">
                        <button>NUEVOS RANGOS</button>
                    </div>

                </div>

            </section>
        </div>

        <%--<img id="imgFondoIndexBody" src="img/fondo-index-body.png" style="width:100%;position:absolute"/>--%>
        <section class="our_latest_product" style="padding-top: 40px">
            <div class="container" id="panel" runat="server" style="display: none;">
                <div class="s_m_title col-12" style="margin-bottom: -55px;">
                    <div class="col-xl-7 col-lg-5 col-md-5 col-sm-5 col-xs-5 scrollflow -opacity" style="margin-left: -30px; margin-top: -7px">
                        <h2>Periodo: Enero</h2>
                    </div>

                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-xs-4 center-block" style="display: none">
                        <h2>Reloj: hh:MM:ss</h2>
                    </div>
                </div>
                <div id="myCarouselNetWorker2" class="carousel slide scrollflow -slide-top" style="margin-top: 100px;" runat="server">
                    <div class="l_product_slider owl-carousel">
                        <div class="item" style="z-index: 300000">
                            <div class="l_product_item cuerpo">
                                <div style="width: 240px; margin: 33.88px auto">
                                    <a href="#x" style="text-align: center">
                                        <h3 style="margin-top: -20px">Rango </h3>
                                        <div style="height: 125px; margin-top: 5px">
                                            <%--<img id="rangoS" src="imagenes/pines/<%=IMG%>" style="width: auto; height: 100%; margin: auto" alt="Image" class="img-responsive">--%>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <%--                    <div class="item">
                        <div class="l_product_item cuerpo">
                            <div style="width: 240px; margin: 33.88px auto">
                                <a href="#x" style="vertical-align: central; text-align: center">
                                    <h3 style="margin-top: 20px">Bonificacion</h3>
                                    <br />
                                    <br />
                                    <h2>S/. 2000.0</h2>
                                </a>
                            </div>
                        </div>
                    </div>--%>
                        <%--<div class="item" style="z-index: 300000">
                            <div class="l_product_item cuerpo">
                                <div style="width: 240px; margin: 33.88px auto">
                                    <a href="#x" style="vertical-align: middle; text-align: center">
                                        <h3>PP</h3>
                                        <br />
                                        <br />
                                        <h2><%=PP %></h2>
                                    </a>
                                </div>

                            </div>
                        </div>
                        <div class="item" style="z-index: 300000">
                            <div class="l_product_item cuerpo">
                                <div style="width: 240px; margin: 33.88px auto">
                                    <a href="#x" style="vertical-align: middle; text-align: center">
                                        <h3 style="margin-top: 20px">VP</h3>
                                        <br />
                                        <br />
                                        <h2><%=VP %></h2>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="item" style="z-index: 300000">
                            <div class="l_product_item cuerpo">
                                <div style="width: 240px; margin: 33.88px auto">
                                    <a href="#x" style="text-align: center">
                                        <h3 style="margin-top: 20px">VQ</h3>
                                        <br />
                                        <br />
                                        <h2><%=VQ %></h2>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="item" style="z-index: 300000">
                            <div class="l_product_item cuerpo">
                                <div style="width: 240px; margin: 33.88px auto">
                                    <a href="#x" style="text-align: center">
                                        <h3 style="margin-top: 20px">Socios Activos Red</h3>
                                        <br />
                                        <br />
                                        <h2><%=SOCIOS %></h2>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="item" style="z-index: 300000">
                            <div class="l_product_item cuerpo">
                                <div style="width: 240px; margin: 33.88px auto">
                                    <a href="#x" style="text-align: center">
                                        <h3 style="margin-top: 20px">Directos Activos</h3>
                                        <br />
                                        <br />
                                        <h2><%=DIRECTOS %></h2>
                                    </a>
                                </div>
                            </div>
                        </div>--%>
                        <%--                    <div class="item">
                        <div class="l_product_item cuerpo">
                            <div style="width: 240px; margin: 14.88px auto">
                                <a href="#x" style="text-align: center">
                                    <h3 style="margin-top: 20px">Consumidores Activos</h3>
                                    <br />
                                    <br />
                                    <h2>3/5</h2>
                                </a>
                            </div>
                        </div>
                    </div>--%>
                        <%--                    <div class="item">
                        <div class="l_product_item cuerpo">
                            <div style="width: 240px; margin: 33.88px auto">
                                <a href="#x" style="text-align: center">
                                    <h3 style="margin-top: 20px">Consultores Activos</h3>
                                    <br />
                                    <br />
                                    <h2>0/0</h2>
                                </a>
                            </div>
                        </div>
                    </div>--%>
                    </div>
                </div>
            </div>
        </section>
        <!--================End Our Latest Product Area =================-->

        <!--================Feature Big Add Area =================-->
        <section class="feature_big_add_area" style="padding-top: 50px">

            <div class="container" style="max-width: 90%; width: 100%">
                <div class="s_m_title col-12 scrollflow -opacity" style="padding-bottom: 2em; margin-left: -15px; display: none">
                    <h2>Publicaciones y novedades</h2>
                </div>
                <div class="w-100"></div>
                <div class="row justify-content-around scrollflow -pop -opacity" style="margin-top: 15px">
                    <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12">
                        <div id="imagen1" class="f_add_item white_add">
                            <div class="f_add_img">
                                <img class="img-fluid" style="width: 100%" src="https://tienda.mundosantanatura.com/img/BANNER-RECETAS-CASERAS.jpg" alt="" />
                            </div>
                            <%--<div id="info-lid" style="display: n">
                                <p id="headline-lid">LLAMADA DE LIDERAZGO</p>
                                <p id="desc-lid">En todo emprendimiento se requiere preparación, esfuerzo y dedicación. Conscientes de ello, los socios líderes de Santa Natura Network realizan capacitaciones o “Llamadas de Liderazgo” los días Domingo a las 9pm a través de la plataforma ZOOM. A través de esta vía, todos los socios del sistema no sólo reciben pautas y sugerencias para el desarrollo de su negocio, también se mantienen informados sobre las novedades de la red.</p>
                            </div>--%>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12">
                        <div id="imagen2" class="f_add_item white_add">
                            <div class="f_add_img">
                                <img class="img-fluid" style="width: 100%" src="https://tienda.mundosantanatura.com/img/BANNER-PRESENTACION-DE-NEGOCIOS.jpg" alt="" />
                            </div>
                            <%--<div id="info-lid">
                                <p id="headline-lid">LLAMADA DE LIDERAZGO</p>
                                <p id="desc-lid">En todo emprendimiento se requiere preparación, esfuerzo y dedicación. Conscientes de ello, los socios líderes de Santa Natura Network realizan capacitaciones o “Llamadas de Liderazgo” los días Domingo a las 9pm a través de la plataforma ZOOM. A través de esta vía, todos los socios del sistema no sólo reciben pautas y sugerencias para el desarrollo de su negocio, también se mantienen informados sobre las novedades de la red.</p>
                            </div>--%>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-lg-offset-0 col-md-6 col-md-offset-2 col-sm-12">
                        <div id="imagen3" class="f_add_item white_add">
                            <div class="f_add_img">
                                <img class="img-fluid" style="width: 100%" src="https://tienda.mundosantanatura.com/img/BANNER-LLAMADA-DE-LIDERAZGO.jpg" alt="" />
                            </div>
                            <%--<div id="info-lid">
                                <p id="headline-lid">LLAMADA DE LIDERAZGO</p>
                                <p id="desc-lid">En todo emprendimiento se requiere preparación, esfuerzo y dedicación. Conscientes de ello, los socios líderes de Santa Natura Network realizan capacitaciones o “Llamadas de Liderazgo” los días Domingo a las 9pm a través de la plataforma ZOOM. A través de esta vía, todos los socios del sistema no sólo reciben pautas y sugerencias para el desarrollo de su negocio, también se mantienen informados sobre las novedades de la red.</p>
                            </div>--%>
                        </div>
                    </div>

                    <%--<div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 scrollflow -pop -opacity ">
                        <div id="imagen2" class="f_add_item white_add">
                            <div class="f_add_img">
                                <img class="img-fluid" style="width: 100%" src="img/FaceLive.jpg" alt="" />
                            </div>
                            <div id="info-live">
                                <p id="headline-live">JEANETTE VÍA LIVE</p>
                                <p id="desc-live">Jeanette Enmanuel, Directora de Santa Natura, comparte todos sus conocimientos con los socios de Santa Natura Network y con las miles de personas e instituciones que recurren a ella como referente de la salud natural en nuestro país. Es por ello que todos los días miércoles a las 10pm (hora local), Jeanette realiza transmisiones vía Facebook Live, a través de la Página Santa Natura Network. Participan amantes de la naturaleza de todo el mundo. ¡Serás Bienvenido!</p>
                            </div>
                        </div>
                    </div>--%>
                    <%--<div class="col-lg-2" style="border-style:solid;border-color:deeppink;width:10px"></div>--%>
                </div>
            </div>
        </section>
        <!--================End Feature Big Add Area =================-->

        <!--================Featured Product Area =================-->
        <section class="feature_product_area" style="padding-top: 50px; padding-bottom: 300px;">
            <div class="container" style="max-width: 100%; padding: 0; width: 100%">
                <div class="row justify-content-around" style="width: 100%; margin: 0;">
                    <!--***************BEGIN VIDEOS SOCIOS DESTACADOS**************-->
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding: 0;">
                        <div class="s_m_title  scrollflow -opacity" style="text-align: center; background: rgba(199,145,57,.2);">
                            <h2 class="claseFuente2" style="font-size: 50px">Videos de Socios Destacados</h2>
                        </div>
                        <br />
                        <br />
                        <div id="videosSocios" class="" style="padding: 20px; max-width: 100%; height: 100%;">

                            <div id="panelVideosSocios" class="row panel panel-info" style="padding: 20px; background: transparent; margin-left: auto; margin-right: auto; border-color: transparent !important; height: 100%; box-shadow: 0 0px 1px rgba(0,0,0,.05);">
                                <div id="myCarousel" style="display: flex; width: 100%; justify-content: space-between; align-items: center" class="" data-ride="">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <span data-slide="prev" data-target="#myCarousel" class="btn-vertical-slider glyphicon glyphicon-circle-arrow-up" style="display: table; transform: scale(2,2) rotate(-90deg)"></span>
                                        </div>
                                    </div>
                                    <%--<br />--%>
                                    <!-- Carousel items -->
                                    <div class="carousel-inner" style="width: 51%; height: 100%;">
                                        <div class="item active" style="height: 100%;">
                                            <div class="row" style="height: 100%;">
                                                <%--<div class="col-sm-8 col-md-8">--%>
                                                <div class="col-xl-7 col-lg-12 col-md-12 col-sm-12" style="">
                                                    <iframe style="display: table; margin-left: auto; margin-right: auto; width: 100%; height: 100%;" src="https://www.youtube.com/embed/dLdpUTS2scw?&autoplay=0" frameborder="0" allowfullscreen></iframe>
                                                </div>
                                                <div class="col-xl-5 col-lg-12 col-md-12 col-sm-12 text-center" style="font-size: 12px; position: relative; margin-top: auto; margin-bottom: auto">
                                                    <p class="claseFuente3">Testimonio de Socia Destacada:</p>
                                                    <p class="claseFuente4">Meryl Portocarrero</p>
                                                    <p class="claseFuente3">Tema:&nbsp<strong>"Visión"</strong></p>

                                                </div>
                                            </div>
                                            <!--/row-fluid-->
                                        </div>
                                        <!--/item-->
                                        <div class="item" style="height: 100%;">
                                            <div class="row" style="height: 100%;">
                                                <div class="col-xl-7 col-lg-12 col-md-12 col-sm-12" style="">
                                                    <iframe style="display: table; margin-left: auto; margin-right: auto; width: 100%; height: 100%;" src="https://www.youtube.com/embed/_6wNro_93bc?autoplay=0&fs=0&iv_load_policy=3&showinfo=0&rel=0&cc_load_policy=0&start=0&end=0&origin=https://youtubeembedcode.com"></iframe>
                                                </div>
                                                <div class="col-xl-5 col-lg-12 col-md-12 col-sm-12 text-center" style="font-size: 12px; margin-top: auto; margin-bottom: auto">
                                                    <p class="claseFuente3">Testimonio de Socio Destacado:</p>
                                                    <p class="claseFuente4">Américo Ticona</p>
                                                    <p class="claseFuente3">Tema:&nbsp<strong>"Positivismo"</strong></p>
                                                </div>
                                            </div>
                                            <!--/row-fluid-->
                                        </div>
                                        <!--/item-->
                                        <!--/item-->
                                        <div class="item" style="height: 100%;">
                                            <div class="row" style="height: 100%;">
                                                <div class="col-xl-7 col-lg-12 col-md-12 col-sm-12" style="">
                                                    <iframe style="display: table; margin-left: auto; margin-right: auto; width: 100%; height: 100%;" src="https://www.youtube.com/embed/u6-b7MGSDiY?autoplay=0&fs=0&iv_load_policy=3&showinfo=0&rel=0&cc_load_policy=0&start=0&end=0&origin=https://youtubeembedcode.com"></iframe>
                                                </div>
                                                <div class="col-xl-5 col-lg-12 col-md-12 col-sm-12 text-center" style="font-size: 12px; margin-top: auto; margin-bottom: auto">
                                                    <p class="claseFuente3">Testimonio de Socio Destacado:</p>
                                                    <p class="claseFuente4">King Roggero</p>
                                                    <p class="claseFuente3">Tema:&nbsp<strong>"Lealtad"</strong></p>
                                                </div>
                                            </div>
                                            <!--/row-fluid-->
                                        </div>
                                        <!--/item-->
                                        <!--/item-->
                                        <div class="item" style="height: 100%;">
                                            <div class="row" style="height: 100%;">
                                                <div class="col-xl-7 col-lg-12 col-md-12 col-sm-12" style="">
                                                    <iframe style="display: table; margin-left: auto; margin-right: auto; width: 100%; height: 100%;" src="https://www.youtube.com/embed/f2WWRjzlKbI?autoplay=0&fs=0&iv_load_policy=3&showinfo=0&rel=0&cc_load_policy=0&start=0&end=0&origin=https://youtubeembedcode.com"></iframe>
                                                </div>
                                                <div class="col-xl-5 col-lg-12 col-md-12 col-sm-12 text-center" style="font-size: 12px; margin-top: auto; margin-bottom: auto">
                                                    <p class="claseFuente3">Testimonio de Socio Destacado:</p>
                                                    <p class="claseFuente4">Alex Céspedes</p>
                                                    <p class="claseFuente3">Tema:&nbsp<strong>"Esfuerzo"</strong></p>
                                                </div>
                                            </div>
                                            <!--/row-fluid-->
                                        </div>
                                        <!--/item-->
                                    </div>
                                    <div class="row" style="padding-top: 20px">
                                        <div class="col-xs-12">
                                            <span data-slide="next" data-target="#myCarousel" class="btn-vertical-slider glyphicon glyphicon-circle-arrow-down" style="display: table; transform: scale(2,2) rotate(-90deg)"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--***************BEGIN CIRCULO**************-->
                    <div id="bloqueSuperAlimentosDestacados" class="col-xl-6 col-lg-6" style="display: none">
                        <div class="s_m_title  scrollflow -opacity">
                            <h2>Súper Alimentos Destacados</h2>
                        </div>
                        <div class="holderCircle  scrollflow -slide-right -opacity">
                            <div class="round"></div>
                            <div class="dotCircle">
                                <span class="itemDot active itemDot1" data-tab="1">
                                    <img style="width: 60px;" src="https://tienda.mundosantanatura.com/imagenes/ACEITE_DE_COCO.png" alt="" />
                                    <span class="forActive"></span>
                                </span>
                                <span class="itemDot itemDot2" data-tab="2">
                                    <img style="width: 60px;" src="https://tienda.mundosantanatura.com/imagenes/ACEITE_DE_COPAIBA.png" alt="" />
                                    <span class="forActive"></span>
                                </span>
                                <span class="itemDot itemDot3" data-tab="3">
                                    <img style="width: 60px;" src="https://tienda.mundosantanatura.com/imagenes/ACEITE_DE_OLIVA.png" alt="" />
                                    <span class="forActive"></span>
                                </span>
                                <span class="itemDot itemDot4" data-tab="4">
                                    <img style="width: 60px;" src="https://tienda.mundosantanatura.com/imagenes/ALGARROBINA_CON_COLAGENO.png" alt="" />
                                    <span class="forActive"></span>
                                </span>
                                <span class="itemDot itemDot5" data-tab="5">
                                    <img style="width: 60px;" src="https://tienda.mundosantanatura.com/imagenes/ALLIN_SURKA.png" alt="" />
                                    <span class="forActive"></span>
                                </span>
                                <span class="itemDot itemDot6" data-tab="6">
                                    <img style="width: 60px;" src="https://tienda.mundosantanatura.com/imagenes/AÑOS_DORADOS.png" alt="" />
                                    <span class="forActive"></span>
                                </span>
                            </div>
                            <div class="contentCircle">
                                <div class="CirItem title-box active CirItem1">
                                    <h3 class="title"><span style="font-family: 'Roboto Slab', serif;">ACEITE DE COCO </span></h3>
                                    <p></p>
                                    <i class="fa">
                                        <img src="https://tienda.mundosantanatura.com/imagenes/ACEITE_DE_COCO.png" alt="" /></i>
                                </div>
                                <div class="CirItem title-box CirItem2">
                                    <h3 class="title"><span style="font-family: 'Roboto Slab', serif;">ACEITE DE COPAIBA </span></h3>
                                    <p></p>
                                    <i class="fa">
                                        <img src="https://tienda.mundosantanatura.com/imagenes/ACEITE_DE_COPAIBA.png" alt="" /></i>
                                </div>
                                <div class="CirItem title-box CirItem3">
                                    <h3 class="title"><span style="font-family: 'Roboto Slab', serif;">ACEITE DE OLIVA</span></h3>
                                    <p></p>
                                    <i class="fa">
                                        <img src="https://tienda.mundosantanatura.com/imagenes/ACEITE_DE_OLIVA.png" alt="" /></i>
                                </div>
                                <div class="CirItem title-box CirItem4">
                                    <h3 class="title"><span style="font-family: 'Roboto Slab', serif; font-size: 15px;">ALGARROBINA CON COLAGENO</span></h3>
                                    <p></p>
                                    <i class="fa">
                                        <img src="https://tienda.mundosantanatura.com/imagenes/ALGARROBINA_CON_COLAGENO.png" alt="" /></i>
                                </div>
                                <div class="CirItem title-box CirItem5">
                                    <h3 class="title"><span style="font-family: 'Roboto Slab', serif;">ALLIN SURKA</span></h3>
                                    <p></p>
                                    <i class="fa">
                                        <img src="https://tienda.mundosantanatura.com/imagenes/ALLIN_SURKA.png" alt="" /></i>
                                </div>
                                <div class="CirItem title-box CirItem6">
                                    <h3 class="title"><span style="font-family: 'Roboto Slab', serif;">AÑOS DORADOS</span></h3>
                                    <p></p>
                                    <i class="fa">
                                        <img src="https://tienda.mundosantanatura.com/imagenes/AÑOS_DORADOS.png" alt="" /></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <%--<section class="feature_product_area" style="display: none">
            <div class="container">
                <section class="our_latest_product" style="position: relative">
                    <div class="container">
                        <div class="s_m_title">
                            <h2>Socios Destacados</h2>
                        </div>
                        <div class="l_product_slider owl-carousel" style="">
                            <% foreach (var producto in listaProductos)
                                { %>
                            <div class="item">
                                <div class="l_product_item">
                                    <div class="l_p_img box21" style="border-top: 0.2px solid darkgray">
                                        <img src="img/socio.jpg" alt="" style="height: 100%" />
                                        <div class="box-content">
                                            <h4 class="title">willimson</h4>
                                            <p class="description">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed mattis augue in odio suscipit, at.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <%} %>
                        </div>
                    </div>
                </section>
            </div>
        </section>--%>
    </div>
    <asp:HiddenField ID="TotalSociosPB" runat="server" />
    <asp:HiddenField ID="ActivosSociosPB" runat="server" />
    <asp:HiddenField ID="VQActualPB" runat="server" />
    <asp:HiddenField ID="VQProximoPB" runat="server" />
    <%--<script src="js/index.js" type="text/javascript"></script>--%>

    <!--Begin Para el banner carrusel de la página Tienda-->
    <script type="text/javascript" src="engine1/wowsliderv2.js"></script>
    <script type="text/javascript" src="engine1/scriptv2.js"></script>
    <!--End Para el banner carrusel de la página Tienda-->



    <!--Para el nuevo Circle Progress Bar-->
    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha384-tsQFqpEReu7ZLhBV2VZlAu7zcOV+rXbYlF2cqB8txI/8aZajjp4Bqd+V6D5IgvKT" crossorigin="anonymous"></script> 

    <script>
        $(function () {

            // Eliminar el estilo en línea svg.radial-progress .complete
            $('svg.radial-progress').each(function (index, value) {
                $(this).find($('circle.complete')).removeAttr('style');
            });

            // Activar la animación de progreso en el desplazamiento
            $(window).ready(function () {
                $('svg.radial-progress').each(function (index, value) {
                    // Si svg.radial-progress es aproximadamente un 25% verticalmente en la ventana cuando se desplaza desde la parte superior o inferior

                    // Obtener porcentaje de progreso
                    percent = $(value).data('percentage');
                    // Obtenga el radio del círculo del svg
                    radius = $(this).find($('circle.complete')).attr('r');
                    // Obtener circunferencia (2πr)
                    circumference = 2 * Math.PI * radius;
                    // Obtener el valor de desplazamiento de trazo - guión basado en el porcentaje de la circunferencia
                    strokeDashOffset = circumference - ((percent * circumference) / 100);
                    // Progreso de la transición durante # segundos
                    $(this).find($('circle.complete')).animate({ 'stroke-dashoffset': strokeDashOffset }, 4550);

                });
            }).trigger('scroll');

        });
    </script>






    <!--Para el anterior Circle Progress Bar-->
 <%--    <script src="js/proyecto2/jquery-3.2.1.js"></script>
   <script src="js/proyecto2/plugin.js"></script>--%>


    <script>
        // Obtiene el modal
        var modal = document.getElementById("modal");

        // Obtiene el elemento <span> que cierra el modal
        var span = document.getElementById("cerrar");

        // Cuando el usuario haga clic en <span> (x), cierre el modal
        span.onclick = function () {
            modal.style.display = "none";
        }

        //Para que al hacer click al cerrar el modal se pausee el video.
        //function toggleVideo(state) {
        //    var div = document.getElementById("popupVid");
        //    var iframe = div.getElementsByTagName("iframe")[0].contentWindow;
        //    div.style.display = state == 'hide' ? 'none' : '';
        //    func = state == 'hide' ? 'pauseVideo' : 'playVideo';
        //    iframe.postMessage('{"event":"command","func":"' + func + '","args":""}', '*');
        //}

        //----------------------------------------------------------------------------------------------------------

        //Para que el menu del navbar se quede de un color cuando esté seleccionado
        window.onload = function () {
            document.getElementById("clicInicio").style.color = 'white';
            document.getElementById("clicInicio").style.borderBottom = '3px solid white';
        }


        $(document).ready(function () {
            $('#myCarousel').carousel({
                interval: false
            })

            //$('#myCarousel').on('slid.bs.carousel', function () {
            //    //alert("slid");
            //});



            /*Para la dimensión del panel de los videos de los socios*/
            var $window = $(window),
                $html = $('#videosSocios');
            $panel = $('#panelVideosSocios');

            $window.resize(function resize() {
                if ($window.width() < 500) {
                    document.getElementById("panelVideosSocios").style.display = "block"
                }

                if ($window.width() > 500) {
                    document.getElementById("panelVideosSocios").style.display = "flex"
                }

                if ($window.width() > 992) {
                    return $html.addClass(' scrollflow -opacity');
                }

                else {
                    $html.removeClass(' scrollflow -opacity');
                    return $html.addClass(' scrollflow -opacity');
                }
            }).trigger('resize');



            /* ---------------------------------------------------------------------------------------------------------------- */
            //Progress bar

            var VQActual = document.getElementById('<%=VQActualPB.ClientID%>');
            var VQProximo = document.getElementById('<%=VQProximoPB.ClientID%>');
            var progress = (VQActual.value / VQProximo.value) * 100;
            $("#valuePercentage").attr("data-percentage", progress);
            //console.log(progress);
<%--            $(".my-progress-bar").circularProgress({
                line_width: 15,
                color: "#262660",
                //height: "300px",
                //width: "300px",
                starting_position: 0, 
                percent: 0,
                percentage: true,
                text: "<%=RANGOPROXIMO %>"
            }).circularProgress('animate', progress, 2000);
            $("#progressbar1").width(progress + '%');
            if (progress % 1 !== 0) {
                $("#progressbar1").text(progress.toFixed(1) + '%');
            } else {
                $("#progressbar1").text(progress + '%');
            }--%>


            var SociosActivos = document.getElementById('<%=ActivosSociosPB.ClientID%>');
            var TotalSocios = document.getElementById('<%=TotalSociosPB.ClientID%>');
            var progress = (SociosActivos.value / TotalSocios.value) * 100;
            //console.log(progress);
            $("#progressbar2").width(progress + '%');
            if (progress % 1 !== 0) {
                $("#progressbar2").text(progress.toFixed(1) + '%');
            } else {
                $("#progressbar2").text(progress + '%');
            }


            var currentCount = 144;
            var progress = (currentCount / 300) * 100;
            //console.log(progress);
            $("#progressbar3").width(progress + '%');
            if (progress % 1 !== 0) {
                $("#progressbar3").text(progress.toFixed(1) + '%');
            } else {
                $("#progressbar3").text(progress + '%');
            }

        });

    </script>

    <%--</div>--%>

    <%--    <script src="js/proyecto2/  theme.js"></script>--%>

    <!--Para el slider de DatosNetWorker-->
    <script src="js/estiloTablaNetWorker.js"></script>



    <!--BEGIN:Para los videos de los socios-->
    <script src="js/proyecto2/estilos-videos.js"></script>

    <%--<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>--%>
    <!--END:Para los videos de los socios-->

    <!--Medallas de Socios Destacados - circle-->
    <%--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>----------------------------------------------------------------------------%>

    <%--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>--%>

    <script src="js/proyecto2/VerticalSlider.js"></script>




    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/proyecto2/popper.min.js"></script>
    <%--<script src="js/proyecto2/bootstrap.min.js"></script>--%>

    <!-- Rev slider js -->
    <script src="css/proyecto2/vendors/revolution/js/jquery.themepunch.tools.min.js"></script>
    <script src="css/proyecto2/vendors/revolution/js/jquery.themepunch.revolution.min.js"></script>

    <script src="css/proyecto2/vendors/revolution/js/extensions/revolution.extension.actions.min.js"></script>
    <script src="css/proyecto2/vendors/revolution/js/extensions/revolution.extension.video.min.js"></script>
    <script src="css/proyecto2/vendors/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
    <script src="css/proyecto2/vendors/revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
    <script src="css/proyecto2/vendors/revolution/js/extensions/revolution.extension.navigation.min.js"></script>
    <script src="css/proyecto2/vendors/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>

    <!-- Extra plugin css -->
    <%--<script src="css/proyecto2/vendors/counterup/jquery.waypoints.min.js"></script>--%>
    <script src="css/proyecto2/vendors/counterup/jquery.counterup.min.js"></script>
    <script src="css/proyecto2/vendors/owl-carousel/owl.carousel.min.js"></script>
    <script src="css/proyecto2/vendors/bootstrap-selector/js/bootstrap-select.min.js"></script>
    <script src="css/proyecto2/vendors/image-dropdown/jquery.dd.min.js"></script>
    <%--<script src="js/proyecto2/smoothscroll.js"></script>--%>

    <script src="css/proyecto2/vendors/isotope/imagesloaded.pkgd.min.js"></script>
    <script src="css/proyecto2/vendors/isotope/isotope.pkgd.min.js"></script>
    <script src="css/proyecto2/vendors/magnify-popup/jquery.magnific-popup.min.js"></script>
    <script src="css/proyecto2/vendors/vertical-slider/js/jQuery.verticalCarousel.js"></script>


    <script src="js/proyecto2/theme.js"></script>


</asp:Content>
