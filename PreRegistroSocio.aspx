﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Home.Master" AutoEventWireup="true" CodeBehind="PreRegistroSocio.aspx.cs" ClientIDMode="Static" Inherits="SantaNaturaNetworkV3.PreRegistroSocio" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="css/Banner de Store Template/animate.css">
    <link rel="stylesheet" type="text/css" href="css/file-upload.css" />
    <link href="css/estilosDetalleCompra.css" rel="stylesheet" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="js/proyecto2/eskju.jquery.scrollflow.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8.8.7/dist/sweetalert2.all.min.js"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <style>
        .anchoBotonGuardar {
            width: 40% !important;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager runat="server">
            <Scripts>
                <asp:ScriptReference Name="WebForms.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebForms.js" />
                <asp:ScriptReference Name="WebUIValidation.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebUIValidation.js" />
                <asp:ScriptReference Name="MenuStandards.js" Assembly="System.Web" Path="~/Scripts/WebForms/MenuStandards.js" />
                <asp:ScriptReference Name="GridView.js" Assembly="System.Web" Path="~/Scripts/WebForms/GridView.js" />
                <asp:ScriptReference Name="DetailsView.js" Assembly="System.Web" Path="~/Scripts/WebForms/DetailsView.js" />
                <asp:ScriptReference Name="TreeView.js" Assembly="System.Web" Path="~/Scripts/WebForms/TreeView.js" />
                <asp:ScriptReference Name="WebParts.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebParts.js" />
                <asp:ScriptReference Name="Focus.js" Assembly="System.Web" Path="~/Scripts/WebForms/Focus.js" />
            </Scripts>
        </asp:ScriptManager>
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
    <div id="ContenidoFluido" class="container-fluid" style="width: 92%; margin-top: 120px">
        <div class="row justify-content-md-center">
            <!--REGISTRO AFILIACION-->
            <div id="MostrarRegistroCliente" class="col-md-8">
                <div class="row form-group colorlib-form">
                    <div class="form-group scrollflow -pop -opacity" style="margin-left: auto; margin-right: auto">
                        <h1>PRE-REGISTRO</h1>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <label style="font-weight: bold; font-size: 16px">DATOS DE LA CUENTA</label>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group col-md-4 scrollflow -opacity">
                                <label class="label" style="font-weight: bold">Usuario</label>
                                <asp:TextBox ID="txtUl" title="Se necesita un nombre de Usuario" MaxLength="8" runat="server" CssClass="form-control text-uppercase marginTop"></asp:TextBox>
                            </div>
                            <div class="form-group col-md-4 scrollflow -opacity">
                                <label class="label" style="font-weight: bold">Clave</label>
                                <asp:TextBox ID="TxtCl" runat="server" CssClass="form-control text-uppercase marginTop" MaxLength="12"></asp:TextBox>
                            </div>
                            <div class="form-group col-md-4 scrollflow -opacity" id="btnCargaImagen" runat="server">
                                <label class="label" style="font-weight: bold">Foto de Perfil</label>
                                <label class="file-upload btn btn-success form-control marginTop" style="font-size: 15px">
                                    Ingresa tu foto
                                    <asp:FileUpload CssClass="form-control imagen" ID="fileUpload" runat="server" />
                                </label>
                            </div>
                        </div>

                        <div class="row col-md-12" id="imagenMostrada" runat="server">
                            <div class="form-group col-md-4 scrollflow -opacity" style="margin-left: auto; margin-right: auto">
                                <label>Mi foto de perfil</label>
                                <div id="imagePreview" class="center-block align-content-center">
                                    <img src="img/usuario1.png" class="img-fluid" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group" style="margin-top: 13px">
                        <label style="font-weight: bold; font-size: 16px">Datos Personales</label>


                        <div class="row col-md-12">
                            <div class="form-group col-md-3 scrollflow -opacity">
                                <label class="label" style="font-weight: bold">UpLine</label>
                                <asp:DropDownList ID="CboUpLine" runat="server" CssClass="form-control btn-lg marginTop" />
                            </div>
                            <div class="form-group col-md-3 scrollflow -opacity">
                                <label class="label" style="font-weight: bold">Patrocinador</label>
                                <asp:TextBox ID="txtPatrocinador" runat="server" CssClass="form-control text-uppercase marginTop" ReadOnly="true"></asp:TextBox>
                            </div>
                            <div class="form-group col-md-3 scrollflow -opacity">
                                <label class="label" style="font-weight: bold">Paquete</label>
                                <asp:DropDownList ID="ddlPaquete" runat="server" CssClass="form-control marginTop" onchange="paqueteCliente(this);">
                                    <asp:ListItem Value="0">Seleccione</asp:ListItem>
                                    <asp:ListItem Value="01">Básico</asp:ListItem>
                                    <asp:ListItem Value="02">Profesional</asp:ListItem>
                                    <asp:ListItem Value="03">Empresarial</asp:ListItem>
                                    <asp:ListItem Value="04">Millonario</asp:ListItem>
                                    <asp:ListItem Value="23">Imperial</asp:ListItem>
                                    <asp:ListItem Value="05">Consultor</asp:ListItem>
                                    <asp:ListItem Value="06">C. Inteligente</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <div class="form-group col-md-3 scrollflow -opacity" style="display:none;">
                                <label class="label" style="font-weight: bold">CDR Preferido</label>
                                <asp:DropDownList ID="cboTipoEstablecimiento" runat="server" CssClass="form-control btn-lg marginTop" />
                            </div>
                        </div>

                        <div class="row col-md-12">
                            <div class="form-group col-md-3 scrollflow -opacity">
                                <label class="label" style="font-weight: bold">Nombres</label>
                                <asp:TextBox ID="txtNombre" runat="server" CssClass="form-control text-uppercase marginTop" onkeydown="validarLetras(event)"
                                    onkeyup="validarLetras(event)"></asp:TextBox>
                            </div>
                            <div class="form-group col-md-3 scrollflow -opacity">
                                <label class="label" style="font-weight: bold">Primer Apellido</label>
                                <asp:TextBox ID="txtApPaterno" runat="server" CssClass="form-control text-uppercase marginTop" onkeydown="validarLetras(event)"
                                    onkeyup="validarLetras(event)"></asp:TextBox>
                            </div>
                            <div class="form-group col-md-3 scrollflow -opacity">
                                <label class="label" style="font-weight: bold">Segundo Apellido</label>
                                <asp:TextBox ID="txtApMaterno" runat="server" CssClass="form-control text-uppercase marginTop" onkeydown="validarLetras(event)"
                                    onkeyup="validarLetras(event)"></asp:TextBox>
                            </div>
                            <div class="form-group col-md-3 scrollflow -opacity">
                                <label class="label" style="font-weight: bold">Nacimiento</label>
                                <input type="text" id="datepicker" class="form-control text-uppercase marginTop" readonly runat="server" />
                            </div>
                        </div>

                        <div class="row col-md-12">
                            <div class="form-group col-md-4 scrollflow -opacity">
                                <label class="label" style="font-weight: bold">Sexo</label>
                                <asp:DropDownList ID="ComboSexo" runat="server" CssClass="form-control marginTop">
                                    <asp:ListItem Value="0">Seleccione</asp:ListItem>
                                    <asp:ListItem Value="1">MASCULINO</asp:ListItem>
                                    <asp:ListItem Value="2">FEMENINO</asp:ListItem>
                                    <asp:ListItem Value="3">NO ESPECIFICA</asp:ListItem>
                                </asp:DropDownList>
                            </div>

                            <div class="form-group col-md-4 scrollflow -opacity">
                                <label class="label" style="font-weight: bold">Tipo documento</label>
                                <asp:DropDownList ID="ComboTipoDocumento" runat="server" CssClass="form-control marginTop">
                                    <asp:ListItem Value="0">Seleccione</asp:ListItem>
                                    <asp:ListItem Value="1">DOCUMENTO DE IDENTIDAD</asp:ListItem>
                                    <asp:ListItem Value="2">PASAPORTE</asp:ListItem>
                                </asp:DropDownList>
                            </div>

                            <div class="form-group col-md-4 scrollflow -opacity">
                                <label class="label" style="font-weight: bold">N° Documento</label>
                                <asp:TextBox ID="txtNumDocumento" runat="server" CssClass="form-control text-uppercase marginTop" onkeypress="return validarNumeros(event)"></asp:TextBox>
                            </div>

                        </div>

                        <div class="row col-md-12">

                            <div class="form-group col-md-4 scrollflow -opacity">
                                <label class="label" style="font-weight: bold">Correo electronico</label>
                                <asp:TextBox ID="TxtCorreo" runat="server" CssClass="form-control text-uppercase marginTop" TextMode="Email"></asp:TextBox>
                            </div>
                            <div class="form-group col-md-4 scrollflow -opacity">
                                <label class="label" style="font-weight: bold">Teléfono</label>
                                <asp:TextBox ID="TxtTelefono" runat="server" onkeypress="return validarNumeros(event)" CssClass="form-control text-uppercase marginTop solo-numero"></asp:TextBox>
                            </div>
                            <div class="form-group col-md-4 scrollflow -opacity">
                                <label class="label" style="font-weight: bold">Celular</label>
                                <asp:TextBox ID="TxtCelular" runat="server" onkeypress="return validarNumeros(event)" CssClass="form-control text-uppercase marginTop solo-numero"></asp:TextBox>
                            </div>
                        </div>
                        <div class="row col-md-12">
                            <div class="form-group col-md-4 scrollflow -opacity">
                                <label class="label" style="font-weight: bold">Pais de operaciones</label>
                                <asp:DropDownList ID="cboPais" runat="server" CssClass="form-control marginTop" />
                            </div>
                            <div class="form-group col-md-4 scrollflow -opacity">
                                <label class="label" style="font-weight: bold">Dirección</label>
                                <asp:TextBox ID="txtDireccion" runat="server" CssClass="form-control text-uppercase marginTop"></asp:TextBox>
                            </div>
                            <div class="form-group col-md-4 scrollflow -opacity">
                                <label class="label" style="font-weight: bold">Referencia de dirección</label>
                                <asp:TextBox ID="TxtReferencia" runat="server" CssClass="form-control text-uppercase marginTop"></asp:TextBox>
                            </div>
                        </div>

                        <div class="row col-md-12">
                            <div class="form-group col-md-4 scrollflow -opacity">
                                <label class="label" style="font-weight: bold">Estado</label>
                                <asp:DropDownList ID="cboDepartamento" runat="server" CssClass="form-control marginTop" />
                            </div>
                            <div class="form-group col-md-4 scrollflow -opacity">
                                <label class="label" style="font-weight: bold">Condado</label>
                                <asp:DropDownList ID="cboProvincia" runat="server" CssClass="form-control marginTop" />
                            </div>
                            <div class="form-group col-md-4 scrollflow -opacity" style="display:none;">
                                <label class="label" style="font-weight: bold">Distrito</label>
                                <asp:DropDownList ID="cboDistrito" runat="server" CssClass="form-control marginTop" />
                                <br />
                            </div>
                            <div class="form-group col-md-4 scrollflow -opacity">
                                <label class="label" style="font-weight: bold">** Código Postal</label>
                                <asp:TextBox ID="txtCodigoPostal" runat="server" CssClass="form-control text-uppercase marginTop" onkeypress="return validarNumeros(event)"></asp:TextBox>
                            </div>
                        </div>

                        <div class="row col-md-12" style="display:none;">
                            <div class="form-group col-md-4 scrollflow -opacity">
                                <label class="label" style="font-weight: bold">CDR Premio</label>
                                <asp:DropDownList ID="cboPremio" runat="server" CssClass="form-control btn-lg marginTop" />
                            </div>
                        </div>

                    </div>
                    <div class="form-group" style="display:none;">
                        <label style="font-weight: bold; font-size: 16px">DATOS BANCARIOS</label>
                        <div class="row col-md-12">
                            <div class="form-group col-md-4 scrollflow -opacity">
                                <label class="label" style="font-weight: bold">RUC</label>
                                <asp:TextBox ID="TxtRUC" runat="server" CssClass="form-control marginTop" onkeypress="return validarNumeros(event)"></asp:TextBox>
                            </div>
                            <div class="form-group col-md-4 scrollflow -opacity">
                                <label class="label" style="font-weight: bold">Banco</label>
                                <asp:TextBox ID="TxtBanco" runat="server" CssClass="form-control text-uppercase marginTop" onkeydown="validarLetras(event)"
                                    onkeyup="validarLetras(event)"></asp:TextBox>
                            </div>
                            <div class="form-group col-md-4 scrollflow -opacity">
                                <label class="label" style="font-weight: bold">N° Cuenta depósito</label>
                                <asp:TextBox ID="TxtNumCuenDeposito" runat="server" CssClass="form-control marginTop" onkeypress="return validarNumeros(event)"></asp:TextBox>
                            </div>
                        </div>

                        <div class="row col-md-12">
                            <div class="form-group col-md-6 scrollflow -opacity">
                                <label class="label" style="font-weight: bold">N° Cuenta detracciones</label>
                                <asp:TextBox ID="TxtNumCuenDetracciones" runat="server" CssClass="form-control marginTop" onkeypress="return validarNumeros(event)"></asp:TextBox>
                            </div>
                            <div class="form-group col-md-6 scrollflow -opacity">
                                <label class="label" style="font-weight: bold">N° Cuenta interbancaria</label>
                                <asp:TextBox ID="TxtNumCuenInterbancaria" runat="server" CssClass="form-control marginTop" onkeypress="return validarNumeros(event)"></asp:TextBox>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="form-group row text-center">
                    <div class="col-lg-12">
                        <button class="btn-lg btn1 btn-success form-controlEPP anchoBotonGuardar" type="button" id="btnRegistrar">GUARDAR</button>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js" type="text/javascript"></script>
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <script src="js/file-uploadv1.js"></script>
    <script src="js/moment.js"></script>
    <script src="js/jquery.numeric.js"></script>
    <script src="js/PreRegistroSocios.js?v5"></script>
    <script type="text/javascript">
        function pageLoad() {
            $('.file-upload').file_upload();
            $('.solo-numero').numeric();

            function filePreview(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $('#imagePreview').html("<img src='" + e.target.result + "' style='height:200px' />");
                    }
                    reader.readAsDataURL(input.files[0]);
                }
            }
            $('.imagen').change(function () {
                filePreview(this);
            });
            $('#datepicker').datepicker({
                uiLibrary: 'bootstrap4'
            });

            function validarLetras(e) {
                var keyCode = (e.keyCode ? e.keyCode : e.which);
                if (keyCode > 47 && keyCode < 58 || keyCode > 95 && keyCode < 107) {
                    e.preventDefault();
                }
            }

            function validarNumeros(evt) {
                var charCode = (evt.which) ? evt.which : event.keyCode;
                if (charCode > 31 && (charCode < 48 || charCode > 57))
                    return false;
                return true;
            }
        }
    </script>

</asp:Content>
