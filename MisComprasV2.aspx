﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Home.Master" AutoEventWireup="true" CodeBehind="MisComprasV2.aspx.cs" Inherits="SantaNaturaNetwork.MisCompras2" ClientIDMode="Static" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <!---->
    <style>
        .datepicker {
            position: absolute !important;
            background-color: white !important;
        }
    </style>
    <%-- --%>
    <%--<link href="css/proyecto2/bootstrap-3.3.0-css-bootstrap.min.css" rel="stylesheet">--%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <link href="css/estilosTablaMisCompras2-v1.css?v1" rel="stylesheet" />
    <link rel="stylesheet" href="css/bootstrapv2.min.css">
    <%--<link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />--%>
    <link href="https://cdn.rawgit.com/atatanasov/gijgo/master/dist/combined/css/gijgo.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="css/file-upload.css" />

    <!---->



    <div class="container-tablaMisCompras2" style="margin-top: 80px">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">

                <nav>
                    <div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
                        <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">COMPRAS PENDIENTES</a>
                        <a class="nav-item nav-link" id="nav-efectivo-tab" data-toggle="tab" href="#nav-pendiente" role="tab" aria-controls="nav-pendiente" aria-selected="false">COMPRAS PENDIENTES EFECTIVO</a>
                        <a class="nav-item nav-link" id="nav-pendpe-tab" data-toggle="tab" href="#nav-pendiente-pe" role="tab" aria-controls="nav-pendiente-pe" aria-selected="false">COMPRAS PENDIENTES PAGOEFECTIVO</a>
                        <a class="nav-item nav-link" id="nav-visa-tab" data-toggle="tab" href="#nav-pendienteVISA" role="tab" aria-controls="nav-visaNet" aria-selected="false">COMPRAS REALIZADAS VISANET</a>
                        <a class="nav-item nav-link" id="nav-rpe-tab" data-toggle="tab" href="#nav-realizadope" role="tab" aria-controls="nav-realizadope" aria-selected="false">COMPRAS REALIZADAS PAGOEFECTIVO</a>
                        <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">COMPRAS REALIZADAS EFECTIVO</a>
                        <a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false">COMPRAS REALIZADAS DEPOSITO</a>
                        <a class="nav-item nav-link" id="nav-about-tab" data-toggle="tab" href="#nav-about" role="tab" aria-controls="nav-about" aria-selected="false">ANULADOS</a>
                    </div>
                </nav>

                <div class="tab-content" id="nav-tabContent">

                    <!-- Compras Pendientes -->
                    <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                        <div id="comprasPen" class="row" style="display: block; padding-bottom: 60px" runat="server">
                            <div class="responsiveTbl table-responsive">
                                <table id="mytable" class="table table-hover table-condensed table-bordered w-auto table2">
                                    <thead class="table-success">
                                        <tr class="text-center" style="color: white">
                                            <th>FECHA PEDIDO</th>
                                            <th>CANTIDAD</th>
                                            <th>MONTO TOTAL NETO</th>
                                            <th>PUNTOS REALES</th>
                                            <th>PUNTOS PROMOCIÓN</th>
                                            <th>VOUCHER</th>
                                            <th>DESPACHO</th>
                                            <th>ESTADO</th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <% if (listaComprasPendientes != null)
                                            {
                                                foreach (var compraPendiente in listaComprasPendientes)
                                                {%>
                                        <tr class="text-center table">
                                            <td hidden><%=compraPendiente.Ticket %></td>
                                            <td><%=compraPendiente.FechaPago.ToString("dd/MM/yyyy") %></td>
                                            <td><%=compraPendiente.Cantidad %></td>
                                            <td><%=compraPendiente.MontoAPagar.ToString("N2").Replace(",", ".") %></td>
                                            <td><%=compraPendiente.PuntosTotal.ToString("N2").Replace(",", ".") %></td>
                                            <td><%=compraPendiente.PuntosTotalPromo.ToString("N2").Replace(",", ".") %></td>
                                            <td><%=compraPendiente.FotoVaucher %></td>
                                            <td><%=compraPendiente.Despacho %></td>
                                            <td>Pendiente</td>
                                            <td style="width: 75px" class="align-middle">
                                                <div>
                                                    <asp:Button ID="BtnEditar" runat="server" OnClientClick="EnviarTicket(this)" OnClick="BtnEditar_Click" CssClass="btn btn-primary btn-lg btn-xs" data-title="Edit" data-toggle="modal" data-target="#edit" Text="Voucher"></asp:Button>
                                                </div>
                                            </td>
                                            <td style="width: 100px" class="align-middle">
                                                <div>
                                                    <asp:Button Text="Ver detalle" ID="BtnDetalleComprasPendientes" OnClientClick="DetalleComprasPendientes(this)" OnClick="BtnDetalleComprasPendientes_Click" runat="server" CssClass="btn btn-success btn-lg btn-xs" data-title="Edit" data-toggle="modal" data-target="#edit"></asp:Button>
                                                </div>
                                            </td>
                                            <td style="width: 75px" class="align-middle">
                                                <div>
                                                    <asp:Button ID="BtnEliminar" runat="server" OnClientClick="EliminarTicket(this)" OnClick="BtnEliminar_Click" CssClass="btn btn-danger btn-lg btn-xs" Text="Eliminar"></asp:Button>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                    <%}
                                        } %>
                                </table>
                            </div>
                        </div>
                    </div>

                    <!-- Compras Pendientes Efectivo-->
                    <div class="tab-pane fade" id="nav-pendiente" role="tabpanel" aria-labelledby="nav-efectivo-tab">
                        <div class="responsiveTbl table-responsive">
                            <table class="table table-hover table-condensed table-bordered w-auto table2">
                                <thead class="table-success">
                                    <tr class="text-center" style="color: white">
                                        <th>FECHA PEDIDO</th>
                                        <th>CANTIDAD</th>
                                        <th>MONTO TOTAL NETO</th>
                                        <th>PUNTOS REALES</th>
                                        <th>PUNTOS PROMOCIÓN</th>
                                        <th>DESPACHO</th>
                                        <th>ESTADO</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody class="text-center table">
                                    <% if (listaComprasPendientesEfectivo != null)
                                        {
                                            foreach (var compraPenEfec in listaComprasPendientesEfectivo)
                                            {%>
                                    <tr class="text-center">
                                        <td hidden><%=compraPenEfec.Ticket %></td>
                                        <td><%=compraPenEfec.FechaPago.ToString("dd/MM/yyyy") %></td>
                                        <td><%=compraPenEfec.Cantidad %></td>
                                        <td><%=compraPenEfec.MontoAPagar.ToString("N2").Replace(",", ".") %></td>
                                        <td><%=compraPenEfec.PuntosTotal.ToString("N2").Replace(",", ".") %></td>
                                        <td><%=compraPenEfec.PuntosTotalPromo.ToString("N2").Replace(",", ".") %></td>
                                        <td><%=compraPenEfec.Despacho %></td>
                                        <td>Pendiente</td>
                                        <td style="width: 100px">
                                            <div class="align-middle">
                                                <asp:Button Text="Ver detalle" ID="DetalleComprasPendienteEfectivo" OnClientClick="DetalleComprasEfectivo(this)" OnClick="DetalleComprasPendienteEfectivo_Click" runat="server" CssClass="btn btn-success btn-lg btn-xs" data-title="Edit" data-toggle="modal"></asp:Button>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                                <%}
                                    } %>
                            </table>
                        </div>
                    </div>

                    <!-- Compras Pendientes Pago Efectivo-->
                    <div class="tab-pane fade" id="nav-pendiente-pe" role="tabpanel" aria-labelledby="nav-pendpe-tab">
                        <div class="responsiveTbl table-responsive">
                            <table class="table table-hover table-condensed table-bordered w-auto table2">
                                <thead class="table-success">
                                    <tr class="text-center" style="color: white">
                                        <th>FECHA PEDIDO</th>
                                        <th>FECHA EXPIRACIÓN</th>
                                        <th>CIP</th>
                                        <th>MONTO TOTAL NETO</th>
                                        <th>PUNTOS REALES</th>
                                        <th>PUNTOS PROMOCIÓN</th>
                                        <th>DESPACHO</th>
                                        <th>ESTADO</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody class="text-center table">
                                    <% if (listaComprasPendientesPagoEfec != null)
                                        {
                                            foreach (var compraPenEfec in listaComprasPendientesPagoEfec)
                                            {%>
                                    <tr class="text-center">
                                        <td hidden><%=compraPenEfec.Ticket %></td>
                                        <td><%=compraPenEfec.FechaCreacion %></td>
                                        <td><%=compraPenEfec.FechaExpiracion %></td>
                                        <td><%=compraPenEfec.CIP %></td>
                                        <td><%=compraPenEfec.MontoAPagar.ToString("N2").Replace(",", ".") %></td>
                                        <td><%=compraPenEfec.PuntosTotal.ToString("N2").Replace(",", ".") %></td>
                                        <td><%=compraPenEfec.PuntosTotalPromo.ToString("N2").Replace(",", ".") %></td>
                                        <td><%=compraPenEfec.Despacho %></td>
                                        <td>Pendiente</td>
                                        <td style="width: 100px">
                                            <div class="align-middle">
                                                <asp:Button Text="Ver detalle" ID="DetalleComprasPendientePagoEfec" OnClientClick="DetalleComprasEfectivo(this)" OnClick="DetalleComprasPendientePagoEfec_Click" runat="server" CssClass="btn btn-success btn-lg btn-xs" data-title="Edit" data-toggle="modal"></asp:Button>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                                <%}
                                    } %>
                            </table>
                        </div>
                    </div>

                    <!--  Compras Realizadas VisaNet -->
                    <div class="tab-pane fade" id="nav-pendienteVISA" role="tabpanel" aria-labelledby="nav-visa-tab">
                        <div class="responsiveTbl table-responsive">
                            <table class="table table-hover table-condensed table-bordered w-auto table2">
                                <thead class="table-success">
                                    <tr class="text-center tr2" style="color: white">
                                        <th class="th2">FECHA PAGO</th>
                                        <th class="th2">CANTIDAD</th>
                                        <th class="th2">MONTO TOTAL NETO</th>
                                        <th class="th2">PUNTOS REALES</th>
                                        <th class="th2">PUNTOS PROMOCIÓN</th>
                                        <th class="th2">DESPACHO</th>
                                        <th class="th2">ESTADO</th>
                                        <th class="th2"></th>
                                    </tr>
                                </thead>
                                <tbody class="text-center table">
                                    <% if (listaComprasRealizadasVisaNet != null)
                                        {
                                            foreach (var comprasRealizadasVisa in listaComprasRealizadasVisaNet)
                                            {%>
                                    <tr class="text-center tr2">
                                        <td class="td2" hidden><%=comprasRealizadasVisa.Ticket %></td>
                                        <td class="td2"><%=comprasRealizadasVisa.FechaPago.ToString("dd/MM/yyyy") %></td>
                                        <td class="td2"><%=comprasRealizadasVisa.Cantidad %></td>
                                        <td class="td2"><%=comprasRealizadasVisa.MontoAPagar.ToString("N2").Replace(",", ".") %></td>
                                        <td class="td2"><%=comprasRealizadasVisa.PuntosTotal.ToString("N2").Replace(",", ".") %></td>
                                        <td class="td2"><%=comprasRealizadasVisa.PuntosTotalPromo.ToString("N2").Replace(",", ".") %></td>
                                        <td class="td2"><%=comprasRealizadasVisa.Despacho %></td>
                                        <td class="td2">Realizado</td>
                                        <td class="td2" style="width: 100px">
                                            <div class="align-middle">
                                                <asp:Button Text="Ver detalle" ID="BtnDetalleComprasRealizadasVisa" OnClientClick="DetalleComprasRealizadas(this)" OnClick="BtnDetalleComprasRealizadasVisa_Click" runat="server" CssClass="btn btn-success btn-lg btn-xs" data-title="Edit" data-toggle="modal"></asp:Button>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                                <%}
                                    } %>
                            </table>
                        </div>
                    </div>

                    <!--  Compras Realizadas Efectivo -->
                    <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                        <div class="responsiveTbl table-responsive">
                            <table class="table table-hover table-condensed table-bordered w-auto table2">
                                <thead class="table-success">
                                    <tr class="text-center tr2" style="color: white">
                                        <th class="th2">FECHA PAGO</th>
                                        <th class="th2">CANTIDAD</th>
                                        <th class="th2">MONTO TOTAL NETO</th>
                                        <th class="th2">PUNTOS REALES</th>
                                        <th class="th2">PUNTOS PROMOCIÓN</th>
                                        <th class="th2">DESPACHO</th>
                                        <th class="th2">ESTADO</th>
                                        <th class="th2"></th>
                                    </tr>
                                </thead>
                                <tbody class="text-center table">
                                    <% if (listaComprasRealizadas != null)
                                        {
                                            foreach (var comprasRealizadas in listaComprasRealizadas)
                                            {%>
                                    <tr class="text-center tr2">
                                        <td class="td2" hidden><%=comprasRealizadas.Ticket %></td>
                                        <td class="td2"><%=comprasRealizadas.FechaPago.ToString("dd/MM/yyyy") %></td>
                                        <td class="td2"><%=comprasRealizadas.Cantidad %></td>
                                        <td class="td2"><%=comprasRealizadas.MontoAPagar.ToString("N2").Replace(",", ".") %></td>
                                        <td class="td2"><%=comprasRealizadas.PuntosTotal.ToString("N2").Replace(",", ".") %></td>
                                        <td class="td2"><%=comprasRealizadas.PuntosTotalPromo.ToString("N2").Replace(",", ".") %></td>
                                        <td class="td2"><%=comprasRealizadas.Despacho %></td>
                                        <td class="td2">Realizado</td>
                                        <td class="td2" style="width: 100px">
                                            <div class="align-middle">
                                                <asp:Button Text="Ver detalle" ID="BtnDetalleComprasRealizadasEfectivoGO" OnClientClick="DetalleComprasRealizadas(this)" OnClick="BtnDetalleComprasRealizadasEfectivoGO_Click" runat="server" CssClass="btn btn-success btn-lg btn-xs" data-title="Edit" data-toggle="modal"></asp:Button>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                                <%}
                                    } %>
                            </table>
                        </div>
                    </div>

                    <!--  Compras Realizadas Pago Efectivo -->
                    <div class="tab-pane fade" id="nav-realizadope" role="tabpanel" aria-labelledby="nav-rpe-tab">
                        <div class="responsiveTbl table-responsive">
                            <table class="table table-hover table-condensed table-bordered w-auto table2">
                                <thead class="table-success">
                                    <tr class="text-center tr2" style="color: white">
                                        <th class="th2">FECHA PEDIDO</th>
                                        <th class="th2">FECHA DE PAGO</th>
                                        <th class="th2">CIP</th>
                                        <th class="th2">MONTO TOTAL NETO</th>
                                        <th class="th2">PUNTOS REALES</th>
                                        <th class="th2">PUNTOS PROMOCIÓN</th>
                                        <th class="th2">DESPACHO</th>
                                        <th class="th2">ESTADO</th>
                                        <th class="th2"></th>
                                    </tr>
                                </thead>
                                <tbody class="text-center table">
                                    <% if (listaComprasRealizadasPagoEfec != null)
                                        {
                                            foreach (var comprasRealizadas in listaComprasRealizadasPagoEfec)
                                            {%>
                                    <tr class="text-center tr2">
                                        <td class="td2" hidden><%=comprasRealizadas.Ticket %></td>
                                        <td class="td2"><%=comprasRealizadas.FechaPago.ToString("dd/MM/yyyy") %></td>
                                        <td class="td2"><%=comprasRealizadas.FechaPagada.Substring(0,10) %></td>
                                        <td class="td2"><%=comprasRealizadas.CIP %></td>
                                        <td class="td2"><%=comprasRealizadas.MontoAPagar.ToString("N2").Replace(",", ".") %></td>
                                        <td class="td2"><%=comprasRealizadas.PuntosTotal.ToString("N2").Replace(",", ".") %></td>
                                        <td class="td2"><%=comprasRealizadas.PuntosTotalPromo.ToString("N2").Replace(",", ".") %></td>
                                        <td class="td2"><%=comprasRealizadas.Despacho %></td>
                                        <td class="td2">Realizado</td>
                                        <td class="td2" style="width: 100px">
                                            <div class="align-middle">
                                                <asp:Button Text="Ver detalle" ID="BtnDetalleComprasRealizadasPagoEfectGO" OnClientClick="DetalleComprasRealizadas(this)" OnClick="BtnDetalleComprasRealizadasPagoEfectGO_Click" runat="server" CssClass="btn btn-success btn-lg btn-xs" data-title="Edit" data-toggle="modal"></asp:Button>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                                <%}
                                    } %>
                            </table>
                        </div>
                    </div>

                    <!--  Compras Realizadas Deposito -->
                    <div class="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">
                        <div class="responsiveTbl table-responsive">
                            <table class="table table-hover table-condensed table-bordered w-auto table2">
                                <thead class="table-success">
                                    <tr class="text-center" style="color: white">
                                        <th>FECHA PAGO</th>
                                        <th>CANTIDAD</th>
                                        <th>MONTO TOTAL NETO</th>
                                        <th>PUNTOS REALES</th>
                                        <th>PUNTOS PROMOCIÓN</th>
                                        <th>VOUCHER</th>
                                        <th>DESPACHO VOUCHER</th>
                                        <th>DESPACHO</th>
                                        <th>ESTADO</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody class="text-center table">
                                    <% if (listaComprasPendientesAprobacion != null)
                                        {
                                            foreach (var compraPenAprob in listaComprasPendientesAprobacion)
                                            {%>
                                    <tr class="text-center">
                                        <td hidden><%=compraPenAprob.Ticket %></td>
                                        <td><%=compraPenAprob.FechaPago.ToString("dd/MM/yyyy") %></td>
                                        <td><%=compraPenAprob.Cantidad %></td>
                                        <td><%=compraPenAprob.MontoAPagar.ToString("N2").Replace(",", ".") %></td>
                                        <td><%=compraPenAprob.PuntosTotal.ToString("N2").Replace(",", ".") %></td>
                                        <td><%=compraPenAprob.PuntosTotalPromo.ToString("N2").Replace(",", ".") %></td>
                                        <td><%=compraPenAprob.FotoVaucher %></td>
                                        <td><%=compraPenAprob.DespachoVoucher %></td>
                                        <td><%=compraPenAprob.Despacho %></td>
                                        <td>Realizado</td>
                                        <td style="width: 100px">
                                            <div class="align-middle">
                                                <asp:Button Text="Ver detalle" ID="Button1" OnClientClick="DetalleComprasRealizadas(this)" OnClick="BtnDetalleComprasRealizadas_Click" runat="server" CssClass="btn btn-success btn-lg btn-xs" data-title="Edit" data-toggle="modal"></asp:Button>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                                <%}
                                    } %>
                            </table>
                        </div>
                    </div>

                    <!--  Compras Anuladas -->
                    <div class="tab-pane fade" id="nav-about" role="tabpanel" aria-labelledby="nav-about-tab">
                        <div class="responsiveTbl table-responsive">
                            <table class="table table-hover table-condensed table-bordered w-auto table2">
                                <thead class="table-success">
                                    <tr class="text-center" style="color: white">
                                        <th>FECHA PAGO</th>
                                        <th>CANTIDAD</th>
                                        <th>MONTO TOTAL NETO</th>
                                        <th>PUNTOS TOTAL</th>
                                        <th>VOUCHER</th>
                                        <th>DESPACHO</th>
                                        <th>ESTADO</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody class="text-center table">
                                    <% if (listaComprasAnuladas != null)
                                        {
                                            foreach (var compraAnula in listaComprasAnuladas)
                                            {%>
                                    <tr class="text-center">
                                        <td hidden><%=compraAnula.Ticket %></td>
                                        <td><%=compraAnula.FechaPago.ToString("dd/MM/yyyy") %></td>
                                        <td><%=compraAnula.Cantidad %></td>
                                        <td><%=compraAnula.MontoAPagar.ToString("N2").Replace(",", ".") %></td>
                                        <td><%=compraAnula.PuntosTotal.ToString("N2").Replace(",", ".") %></td>
                                        <td><%=compraAnula.FotoVaucher %></td>
                                        <td><%=compraAnula.Despacho %></td>
                                        <td>Realizado</td>
                                        <td style="width: 100px">
                                            <div class="align-middle">
                                                <asp:Button Text="Ver detalle" ID="Button2" OnClientClick="DetalleComprasRealizadas(this)" OnClick="BtnDetalleComprasRealizadas_Click" runat="server" CssClass="btn btn-success btn-lg btn-xs" data-title="Edit" data-toggle="modal"></asp:Button>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                                <%}
                                    } %>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <!-- Modal editar pendiendtes -->
    <div class="modal" id="editarComprasPendientes" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title font-weight-bold">EDITAR COMPRA</h5>
                    <button type="button" onclick="QuitarCamposRequeridos()" class="close" data-dismiss="modal" aria-label="Close">
                        <span onclick="QuitarCamposRequeridos()" aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="form-group container text-center">
                            <h5 class="font-weight-bold title">MI VOUCHER</h5>
                            <div id="imagePreview" class="center-block align-content-center">
                                <img src="imagenes/otherImagenes/voucher-default.png" class="img-fluid" />
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group container">
                            <label class="col-lg-3 col-form-label form-control-label">Voucher</label>
                            <div class="col-lg-9">
                                <label class="file-upload btn btn-success form-control">
                                    subir voucher
                   
                                    <input type="file" class="imagen form-control" id="imagen" name="NuevaFotoCliente" accept="image/x-png,image/jpeg" runat="server" />
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label class="col-sm-3 col-form-label">Banco</label>
                            <div class="col-sm-8">
                                <select id="cboBanco" runat="server" class="form-control">
                                    <option hidden value="0">Seleccione</option>
                                    <option value="1">BCP</option>
                                    <option value="2">BBVA RECAUDO</option>
                                    <option value="3">BBVA</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group col-md-6">
                            <label class="col-sm-3 col-form-label">N° Operación</label>
                            <div class="col-sm-8">
                                <input type="text" onkeypress="return validarNumeros(event)" class="form-control" id="TxtNumOperacion" runat="server" placeholder="Ingresar N° opéración">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label class="col-sm-3 col-form-label">Monto</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control solo-numero" id="TxtMonto" runat="server" placeholder="Ingresar monto" readonly>
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="col-sm-3 col-form-label">Fecha voucher</label>
                            <div class="col-sm-8">
                                <%--<input type="text" id="datepicker" class="btn-lg" runat="server" readonly />--%>
                                <div class="input-group date" id="datetimepicker2">
                                    <input id="datepicker" type="text" class="form-control" runat="server" readonly ><span class="input-group-addon"><i class="glyphicon glyphicon-th" style="right: 6px"></i></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label class="col-sm-3 col-form-label">Tipo de Pago</label>
                            <div class="col-sm-8">
                                <select id="cboTipoPago" name="cboTPago" runat="server" class="form-control">
                                    <option hidden value="0">Seleccione</option>
                                    <option value="1">TRANSFERENCIA</option>
                                    <option value="2">DEPOSITO</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="col-sm-3 col-form-label">Confirmar Tienda:</label>
                            <div class="col-sm-8">
                                <asp:DropDownList ID="ComboTienda" CssClass="form-control btn-lg" runat="server" Enabled="false" Font-Size="10px">
                                </asp:DropDownList>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label class="col-sm-3 col-form-label">Tipo de Comprobante</label>
                            <div class="col-sm-8">
                                <select id="cboComprobante" runat="server" class="form-control">
                                    <option hidden value="0">Seleccione</option>
                                    <option value="1">Boleta</option>
                                    <option value="2">Factura</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group container">
                            <label class="col-lg-3 col-form-label form-control-label"></label>
                            <div class="col-lg-9">
                                <asp:Button ID="BtnAgregarVaucher" OnClick="BtnAgregarVaucher_Click" Text="Guardar" CssClass="btn btn-lg btn-success float-right" runat="server" />
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <div class="col-md-12">
                        <button type="button" class="btn btn-lg btn-danger" onclick="QuitarCamposRequeridos()" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal realizadas -->
    <div class="modal" id="comprasRealizadasModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title font-weight-bold">DETALLE DE MI COMPRA</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body scroll style-1">
                    <div class="row">
                        <div class="col-12 col-sm-7 col-md-7">
                            <div class="table-responsive">
                                <table class="table table-bordred table-striped">
                                    <thead class="table-success">
                                        <tr class="text-center">
                                            <th>SUPER ALIMENTO</th>
                                            <th>NOMBRE</th>
                                            <th>CANTIDAD</th>
                                        </tr>
                                    </thead>
                                    <tbody class="text-center">
                                        <% if (listaDetalleCompra != null)
                                            {
                                                foreach (var detalleCompra in listaDetalleCompra)
                                                {%>
                                        <tr class="text-center">
                                            <td style="width: 20%">
                                                <div class="row">
                                                    <div class="col-9 col-sm-9 col-md-9 center-block">
                                                        <img src="products/<%=detalleCompra.Foto %>" class="img-responsive" />
                                                    </div>
                                                </div>
                                            </td>
                                            <td><%=detalleCompra.Nombre %></td>
                                            <td><%=detalleCompra.Cantidad %></td>
                                        </tr>
                                    </tbody>
                                    <%}
                                        } %>
                                </table>
                            </div>
                        </div>
                        <div class="col-12 col-sm-5 col-md-5">
                            <div style="height: 250px">
                                <% if (listaDetalleCompra != null)
                                    {%>
                                <img src="<%=fotoVoucherCompra%>" class="img-responsive" />
                                <%} %>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-lg btn-danger" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal realizadas Efectivo-->
    <div class="modal" id="comprasRealizadasModalEfectivoGo" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title font-weight-bold">DETALLE DE MI COMPRA</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-12 col-sm-12 col-md-12">
                            <div class="table-responsive">
                                <table class="table table-bordred table-striped">
                                    <thead class="table-success">
                                        <tr class="text-center">
                                            <th>SUPER ALIMENTO</th>
                                            <th>NOMBRE</th>
                                            <th>CANTIDAD</th>
                                        </tr>
                                    </thead>
                                    <tbody class="text-center">
                                        <% if (listaDetalleCompra != null)
                                            {
                                                foreach (var detalleCompra in listaDetalleCompra)
                                                {%>
                                        <tr class="text-center">
                                            <td style="width: 20%">
                                                <div class="row">
                                                    <div class="col-9 col-sm-9 col-md-9 center-block">
                                                        <img src="products/<%=detalleCompra.Foto %>" class="img-responsive" />
                                                    </div>
                                                </div>
                                            </td>
                                            <td><%=detalleCompra.Nombre %></td>
                                            <td><%=detalleCompra.Cantidad %></td>
                                        </tr>
                                    </tbody>
                                    <%}
                                        } %>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-lg btn-danger" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal realizadas Visa Net-->
    <div class="modal" id="comprasRealizadasVisaModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title font-weight-bold">DETALLE DE MI COMPRA</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-12 col-sm-12 col-md-12">
                            <div class="table-responsive">
                                <table class="table table-bordred table-striped">
                                    <thead class="table-success">
                                        <tr class="text-center">
                                            <th>SUPER ALIMENTO</th>
                                            <th>NOMBRE</th>
                                            <th>CANTIDAD</th>
                                        </tr>
                                    </thead>
                                    <tbody class="text-center">
                                        <% if (listaDetalleCompra != null)
                                            {
                                                foreach (var detalleCompra in listaDetalleCompra)
                                                {%>
                                        <tr class="text-center">
                                            <td style="width: 20%">
                                                <div class="row">
                                                    <div class="col-9 col-sm-9 col-md-9 center-block">
                                                        <img src="products/<%=detalleCompra.Foto %>" class="img-responsive" />
                                                    </div>
                                                </div>
                                            </td>
                                            <td><%=detalleCompra.Nombre %></td>
                                            <td><%=detalleCompra.Cantidad %></td>
                                        </tr>
                                    </tbody>
                                    <%}
                                        } %>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-lg btn-danger" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal Pendiente Efectivo -->
    <div class="modal" id="comprasPendientesEfectivoModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title font-weight-bold">DETALLE DE MI COMPRA</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-12 col-sm-12 col-md-12">
                            <div class="table-responsive">
                                <table class="table table-bordred table-striped">
                                    <thead class="table-success">
                                        <tr class="text-center">
                                            <th>SUPER ALIMENTO</th>
                                            <th>NOMBRE</th>
                                            <th>CANTIDAD</th>
                                        </tr>
                                    </thead>
                                    <tbody class="text-center">
                                        <% if (listaDetalleCompra != null)
                                            {
                                                foreach (var detalleCompra in listaDetalleCompra)
                                                {%>
                                        <tr class="text-center">
                                            <td style="width: 20%">
                                                <div class="row">
                                                    <div class="col-9 col-sm-9 col-md-9 center-block">
                                                        <img src="products/<%=detalleCompra.Foto %>" class="img-responsive" />
                                                    </div>
                                                </div>
                                            </td>
                                            <td><%=detalleCompra.Nombre %></td>
                                            <td><%=detalleCompra.Cantidad %></td>
                                        </tr>
                                    </tbody>
                                    <%}
                                        } %>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer ">
                    <button type="button" class="btn btn-lg btn-danger" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal Pendiente Pago Efectivo -->
    <div class="modal" id="comprasPendientesPagoEfecModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title font-weight-bold">DETALLE DE MI COMPRA</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-12 col-sm-12 col-md-12">
                            <div class="table-responsive">
                                <table class="table table-bordred table-striped">
                                    <thead class="table-success">
                                        <tr class="text-center">
                                            <th>SUPER ALIMENTO</th>
                                            <th>NOMBRE</th>
                                            <th>CANTIDAD</th>
                                        </tr>
                                    </thead>
                                    <tbody class="text-center">
                                        <% if (listaDetalleCompra != null)
                                            {
                                                foreach (var detalleCompra in listaDetalleCompra)
                                                {%>
                                        <tr class="text-center">
                                            <td style="width: 20%">
                                                <div class="row">
                                                    <div class="col-9 col-sm-9 col-md-9 center-block">
                                                        <img src="products/<%=detalleCompra.Foto %>" class="img-responsive" />
                                                    </div>
                                                </div>
                                            </td>
                                            <td><%=detalleCompra.Nombre %></td>
                                            <td><%=detalleCompra.Cantidad %></td>
                                        </tr>
                                    </tbody>
                                    <%}
                                        } %>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer ">
                    <button type="button" class="btn btn-lg btn-danger" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal Detalle pendientes -->
    <div class="modal" id="comprasPendientesModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title font-weight-bold">DETALLE DE MI COMPRA</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-12 col-sm-12 col-md-12">
                            <div class="table-responsive">
                                <table class="table table-bordred table-striped">
                                    <thead class="table-success">
                                        <tr class="text-center">
                                            <th>SUPER ALIMENTO</th>
                                            <th>NOMBRE</th>
                                            <th>CANTIDAD</th>
                                        </tr>
                                    </thead>
                                    <tbody class="text-center">
                                        <% if (listaDetalleCompra != null)
                                            {
                                                foreach (var detalleCompra in listaDetalleCompra)
                                                {%>
                                        <tr class="text-center">
                                            <td style="width: 20%">
                                                <div class="row">
                                                    <div class="col-9 col-sm-9 col-md-9 center-block">
                                                        <img src="products/<%=detalleCompra.Foto %>" class="img-responsive" />
                                                    </div>
                                                </div>
                                            </td>
                                            <td><%=detalleCompra.Nombre %></td>
                                            <td><%=detalleCompra.Cantidad %></td>
                                        </tr>
                                    </tbody>
                                    <%}
                                        } %>
                                </table>
                            </div>
                        </div>
                        <div class="col-12 col-sm-5 col-md-5">
                            <div style="height: 250px">
                                <% if (listaDetalleCompra != null)
                                    {%>
                                <img src="<%=fotoVoucherCompra%>" class="img-responsive" />
                                <%} %>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer ">
                    <button type="button" class="btn btn-lg btn-danger" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>

    <asp:HiddenField ID="Ticket" runat="server" />

    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="js/bootstrap4.min.js"></script>
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>


    <script src="js/file-uploadv1.js"></script>
    <script src="js/jquery.numeric.js"></script>
    <%--<script src="js/bootstrap.bundle.min.js"></script>--%>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>

    <!--Nuevo DatePicker (calendario)-->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.min.js"></script>
    <!--Nuevo DatePicker (calendario) en español-->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/locales/bootstrap-datepicker.es.min.js"></script>

    <script>
        window.onload = function () {
            document.getElementById("idMenuTienda").style.color = 'white';
            document.getElementById("idMenuTienda").style.borderBottom = '3px solid white';
            document.getElementById("idSubMenuHistorialCompra").style.color = 'white';
            document.getElementById("idSubMenuHistorialCompra").style.borderBottom = '3px solid white';
        }

        $('#datetimepicker2').datepicker({
            weekStart: 0,
            todayBtn: "linked",
            language: "es",
            orientation: "bottom auto",
            keyboardNavigation: false,
            autoclose: true
        });
    </script>
    <script>
        $("[id$=datepicker]").datepicker({
            uiLibrary: 'bootstrap4',
            format: 'dd/mm/yyyy',
            endDate: new Date()
        });
        $('.solo-numero').numeric();

        function showRowComprasByID(value, rowComprasPen, rowComprasRea, rowComprasPenAprob, rowComprasAnula) {
            if (value == "1") {
                rowComprasPen.style.display = "block";
                rowComprasRea.style.display = "none";
                rowComprasPenAprob.style.display = "none";
                rowComprasAnula.style.display = "none";
            } else if (value == "2") {
                rowComprasPen.style.display = "none";
                rowComprasRea.style.display = "block";
                rowComprasPenAprob.style.display = "none";
                rowComprasAnula.style.display = "none";
            } else if (value == "3") {
                rowComprasPen.style.display = "none";
                rowComprasRea.style.display = "none";
                rowComprasPenAprob.style.display = "block";
                rowComprasAnula.style.display = "none";
            } else {
                rowComprasPen.style.display = "none";
                rowComprasRea.style.display = "none";
                rowComprasPenAprob.style.display = "none";
                rowComprasAnula.style.display = "block";
            }
        }

        function EnviarTicket(t) {
            var tick = $($($(t).parent().parent().children()[0]).children()[0]).parent().parent().parent().parent().children()[0].children[0].textContent;
            var ticket = $("[id$=Ticket]");
            ticket.val(tick);
        }

        function EliminarTicket(t) {
            var tick = $($($(t).parent().parent().children()[0]).children()[0]).parent().parent().parent().parent().children()[0].children[0].textContent;
            var ticket = $("[id$=Ticket]");
            ticket.val(tick);
        }

        function DetalleComprasRealizadas(t) {
            var tick = $($($(t).parent().parent().children()[0]).children()[0]).parent().parent().parent().parent().children()[0].children[0].textContent;
            var ticket = $("[id$=Ticket]");
            ticket.val(tick);
        }

        function DetalleComprasEfectivo(t) {
            var tick = $($($(t).parent().parent().children()[0]).children()[0]).parent().parent().parent().parent().children()[0].children[0].textContent;
            var ticket = $("[id$=Ticket]");
            ticket.val(tick);
        }

        function DetalleComprasPendientes(t) {
            var tick = $($($(t).parent().parent().children()[0]).children()[0]).parent().parent().parent().parent().children()[0].children[0].textContent;
            var ticket = $("[id$=Ticket]");
            ticket.val(tick);
        }

        function MostrarComprasRealizadas() {
            $("#comprasRealizadasModal").modal("show");
        }

        function MostrarComprasRealizadasEfectivo() {
            $("#comprasRealizadasModalEfectivoGo").modal("show");
        }

        function MostrarComprasRealizadasVisa() {
            $("#comprasRealizadasVisaModal").modal("show");
        }

        function MostrarComprasPendientesEfectivo() {
            $("#comprasPendientesEfectivoModal").modal("show");
        }

        function MostrarComprasPendientesPagoEfec() {
            $("#comprasPendientesPagoEfecModal").modal("show");
        }

        function MostrarComprasPendientes() {
            $("#comprasPendientesModal").modal("show");
        }

        function ShowModalEditar() {
            FijarCamposRequeridos();
            $("#editarComprasPendientes").modal("show");
        }

        function FijarCamposRequeridos() {
            $("[id$=TxtBanco]").prop("required", true);
            $("[id$=TxtNumOperacion]").prop("required", true);
            $("[id$=datepicker]").prop("required", true);
            $("[id$=TxtMonto]").prop("required", true);
            $("[id$=imagen]").prop("required", true);
            $("[id$=ComboTienda]").prop("required", true);
            $("[id$=cboComprobante]").prop("required", true);
        }

        function QuitarCamposRequeridos() {
            $("[id$=TxtBanco]").prop("required", false);
            $("[id$=TxtNumOperacion]").prop("required", false);
            $("[id$=datepicker]").prop("required", false);
            $("[id$=TxtMonto]").prop("required", false);
            $("[id$=imagen]").prop("required", false);
            $("[id$=ComboTienda]").prop("required", false);
            $("[id$=cboComprobante]").prop("required", false);
        }

        function pageLoad() {
            $('.file-upload').file_upload();

            function filePreview(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $('#imagePreview').html("<img src='" + e.target.result + "' style='height:200px' />");
                    }
                    reader.readAsDataURL(input.files[0]);
                }
            }
            $('.imagen').change(function () {
                filePreview(this);
            });
        }

        function validarLetras(e) {
            var keyCode = (e.keyCode ? e.keyCode : e.which);
            if (keyCode > 47 && keyCode < 58 || keyCode > 95 && keyCode < 107) {
                e.preventDefault();
            }
        }

        function validarNumeros(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
            return true;
        }
    </script>
</asp:Content>
