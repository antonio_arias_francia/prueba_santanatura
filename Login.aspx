﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="SantaNaturaNetwork.Login" %>

<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" />
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Inicio de Sesión</title>

    <link rel="icon" href="https://tienda.mundosantanatura.com/img/Natural_Food_icon.png" type="image/x-icon" />
    <!-- Dışarıdan Çağırılan Dosyalar Font we Materyal İkonlar -->
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,700' rel='stylesheet' type='text/css' />
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" />
    <!-- #Dışarıdan Çağırılan Dosyalar Font we Materyal İkonlar Bitiş -->
    <link href="css/estilos-login5.css" rel="stylesheet" type='text/css' />
    <%--<link href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" rel="stylesheet" type="text/css" />--%>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

    <link href="css/bootstrap-sweetalert-master/sweetalert.css" rel="stylesheet" />
    <script src="js/bootstrap-sweetalert-master/sweetalert.min.js" type="text/javascript"></script>


    <style>
        .passwordLogin {
            width: 88%
        }

        #password_visibility {
            width: 12%
        }

        .claseFuente1 {
            font-size: 60px;
            font-family: 'HurmeGeometricSans1 BlackOblique';
            color: #1B1464;
        }

        .claseFuente2 {
            font-size: 30px;
            font-family: 'HurmeGeometricSans1';
            color: #1B1464;
            font-weight: bold;
        }

        .claseFuente3 {
            font-size: 15px;
            font-family: 'HurmeGeometricSans1 Oblique';
            color: #1B1464 !important;
            font-weight: bold !important;
        }

        .claseFuente4 {
            font-size: 10px;
            font-family: 'HurmeGeometricSans1 Oblique';
            color: #1B1464;
            font-weight: bold;
            margin-left: -95px;
        }

        .claseFuente5 {
            font-size: 20px !important;
            font-family: 'HurmeGeometricSans1 Black';
            color: #1B1464 !important;
            font-weight: bold;
        }
    </style>
    <link href="css/proyecto2/fonts-v2.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <div id="fondoFormularioLogin">
        <div class=" login-card">
            <form id="form1" runat="server" class="col-lg-12" style="margin-bottom: auto; margin-top: auto;">
                <asp:Login ID="LoginUser" runat="server" EnableViewState="false" OnAuthenticate="LoginUser_Authenticate" Width="100%">
                    <LayoutTemplate>
                        <!-- Logo -->
                        <div class="col-lg-12 logo-kapsul">
                            <img id="imgLogoParaLogin" width="250" class="logo" src="https://tienda.mundosantanatura.com/img/LOGO-PARA-LOGIN-NEW.png" alt="Logo" style="padding-top: -40px; padding-bottom: -40px" />
                            <br />
                            <%--<img width="110" src="https://dqcgrsy5v35b9.cloudfront.net/cruiseplanner/assets/img/icons/login-w-icon.png" style="top: 20px; padding-bottom: 50px; padding-top: -30px" />--%>
                            <%--<img id="bienvenidosLogin" width="250" src="img/bienvenidos-login.png" style="top: 20px; padding-bottom: 50px" />--%>
                            <div>
                                <p class="claseFuente1">¡Bienvenidos!</p>
                            </div>
                            <br />
                            <h4 class="claseFuente2" id="ingreseDatos">Ingrese sus datos para continuar</h4>
                            <br />
                            <div id="bloqueNoEresEmpresario" style="display: none; justify-content: space-evenly; align-items: center;">
                                <div>
                                    <h6 class="claseFuente3" id="noEresEmpresario">¿NO ERES EMPRESARIO SNN? &nbsp;&nbsp;</h6>
                                </div>
                                <div>
                                    <a class="claseFuente4" id="ClickAqui" href="#">HAGA CLICK AQUI</a>
                                </div>
                            </div>
                        </div>
                        <!-- #Logo Bitiş -->

                        <div style="clear: both"></div>

                        <!-- Kullanıcı Adı Giriş İnput -->
                        <div class="group" id="campoUsuario" style="margin-top: 30px">
                            <%--<input type="text" required="required" runat="server" name="usuario" id="usuario"/>--%>
                            <div style="width: 99px; margin-right: auto">
                                <label style="position: inherit"><%--<i id="iconoLabelUsuario" class="material-icons input-ikon">person_outline</i>--%><span id="labelUsuario" class="span-input claseFuente5">USUARIO</span></label>
                            </div>

                            <asp:TextBox ID="UserName" runat="server" placeholder="Ingrese usuario..." MaxLength="8"></asp:TextBox>
                        </div>
                        <!-- #Kullanıcı Adı Giriş İnput Bitiş -->

                        <!-- Şifre İnput Giriş-->
                        <div class="group" id="campoClave">
                            <%--<input type="password" required="required" runat="server" name="clave" id="clave"/>--%>
                            <div style="width: 99px; margin-right: auto">
                                <label style="position: inherit"><%--<i id="iconoLabelClave" class="material-icons input-sifre-ikon">lock</i>--%><span id="labelClave" class="span-input text-center claseFuente5">CLAVE</span></label>
                            </div>
                            <div class="pass_show" style="display: flex; align-items: center">
                                <asp:TextBox CssClass="passwordLogin" TextMode="Password" ID="Password" runat="server" placeholder="Ingrese clave..." MaxLength="12"></asp:TextBox>
                            </div>
                        </div>
                        <!-- Şifre İnput Giriş Bitiş-->

                        <!-- Giriş Yap Buton -->
                        <%--<a href="Index.aspx" class="giris-yap-buton">Iniciar Sesión</a>--%>
                        <div id="iniciarSesion" class="group">
                            <asp:Button ID="Button1" CommandName="Login" runat="server" Text="INICIAR SESIÓN" CssClass="giris-yap-buton claseFuente5" />
                        </div>
                        <!-- #Giriş Yap Buton Bitiş -->

                        <!-- Şifremi Unuttum ve Kaydol Linkleri -->
                        <div class="forgot-and-create tab-menu" style="display:none;">
                            <a id="signoInterrogacion1" class="claseFuente3" style="font-size: 15px; margin-right: -1px">¿</a><a class="sifre-hatirlat-link claseFuente3" href="javascript:void('sifre-hatirlat-link');">Olvidaste tu contraseña</a><a id="signoInterrogacion2" class="claseFuente3" style="font-size: 15px; margin-left: -10px">?</a>
                            <%--<a class="hesap-olustur-link" href="javascript:void('hesap-olustur-link');" style="font-size: 15px; float: right; color: aquamarine">Crear cuenta</a>--%>
                        </div>
                        <!-- Şifremi Unuttum ve Kaydol Linkleri Bitiş -->
                    </LayoutTemplate>
                </asp:Login>
            </form>

            <!-- Sifre Hatirlat Form Sayfası -->
            <form id="sifre-hatirlat-form" class="col-lg-12">

                <div class="col-lg-12 logo-kapsul">
                    <img width="110" class="logo" src="https://tienda.mundosantanatura.com/img/contraseña.png" alt="Logo" />
                </div>

                <div style="clear: both"></div>

                <!-- Şifre Hatırlat Email İnput -->
                <div class="group">
                    <input type="text" required="required" />
                    <span class="highlight"></span>
                    <span class="bar"></span>
                    <label><i class="material-icons input-ikon">mail_outline</i><span class="span-input">E-Mail</span></label>
                </div>
                <!-- #Şifre Hatırlat Email İnput Bitiş -->

                <!-- Şifremi Hatırlat Buton -->

                <div id="btnEnviarCorreoLogin" class="group">
                    <a href="javascript:void(0);" class="giris-yap-buton claseFuente5" style="text-decoration: none !important">Enviar</a>
                </div>
                <!-- #Şifremi Hatırlat Buton Bitiş -->

                <!-- Zaten Hesap Var Link -->
                <a class="zaten-hesap-var-link claseFuente3" href="javascript:void('zaten-hesap-var-link');">¿Ya tienes una cuenta? Inicia sesión.</a>
                <!-- #Zaten Hesap Var Link Bitiş -->

            </form>

            <%--<form id="kayit-form" class="col-lg-12">
                <div class="col-lg-12 logo-kapsul">
                    <img width="100" class="logo" src="https://dqcgrsy5v35b9.cloudfront.net/cruiseplanner/assets/img/icons/login-w-icon.png" alt="Logo" />
                </div>

                <div style="clear: both;"></div>

                <!-- Kayıt Form Kullanıcı Adı İnput -->
                <div class="group">
                    <input type="text" required="required" />
                    <span class="highlight"></span>
                    <span class="bar"></span>
                    <label><i class="material-icons input-ikon">person_outline</i><span class="span-input">Su nombre de usuario</span></label>
                </div>
                <!-- #Kayıt Form Kullanıcı Adı İnput Bitiş -->

                <!-- Kayıt Form Email İnput -->
                <div class="group">
                    <input type="text" required="required" />
                    <span class="highlight"></span>
                    <span class="bar"></span>
                    <label><i class="material-icons input-ikon">mail_outline</i><span class="span-input">E-Mail</span></label>
                </div>
                <!-- #Kayıt Form Email İnput Bitiş -->

                <!-- Kayıt Form Şifre İnput -->
                <div class="group">
                    <input type="password" required="required" />
                    <span class="highlight"></span>
                    <span class="bar"></span>
                    <label><i class="material-icons input-sifre-ikon">lock</i><span class="span-input">Contraseña</span></label>
                </div>
                <!-- #Kayıt Form Şifre İnput Bitiş -->

                <!-- Kayıt Form Şifre-Tekrarı İnput -->
                <div class="group">
                    <input type="password" required="required" />
                    <span class="highlight"></span>
                    <span class="bar"></span>
                    <label><i class="material-icons input-sifre-ikon">lock</i><span class="span-input">Repetir contraseña</span></label>
                </div>
                <!-- #Kayıt Form Şifre-Tekrarı İnput Bitiş -->

                <!-- Kayıt Ol Buton -->
                <a href="javascript:void(0);" class="giris-yap-buton">Crear una cuenta</a>
                <!-- #Kayıt Ol Buton Bitiş -->

                <!-- Zaten Hesap Var Link -->
                <a class="zaten-hesap-var-link" href="javascript:void('zaten-hesap-var-link');">¿Ya tienes una cuenta? Inicia sesión.</a>
                <!--# Zaten Hesap Var Link Bitiş -->
            </form>--%>
            <!-- ##Kayıt Form  Sayfası Bitis -->
        </div>
    </div>


    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    <script src="js/estilos-login3.js"></script>

    <script>

        $(document).ready(function () {

            var obtenerAnchoPagina = $("#fondoFormularioLogin").width();
            console.log("obtenerAnchoPagina: " + obtenerAnchoPagina);

        });

    </script>

</body>
</html>
